import React, { useState, useEffect } from 'react';
import { View, Text } from 'react-native';

import { getSingleCustomer } from '../firebase/customers';
import { getSingleEmployee } from '../firebase/employees';

const SingleCustomer = ({ route }: any) => {
  const [customer, setCustomer] = useState();
  const [employee, setEmployee] = useState();
  const { id } = route.params;

  const init = async () => {
    const result = await getSingleCustomer(id);
    if (result && result.Employee) {
      const emp = await getSingleEmployee(result.Employee);
      await setEmployee(emp);
    }
    await setCustomer(result);
  };

  useEffect(() => {
    init();
  }, []);
  return (
    <>
      {customer && (
        <View>
          {customer.FirstName && (
            <Text>
              {customer.FirstName} {customer.LastName}
            </Text>
          )}
          {customer.Address && <Text>Address: {customer.Address}</Text>}
          {employee && (
            <Text>
              Employee: {employee.FirstName} {employee.LastName}
            </Text>
          )}
          {customer.Phone && <Text>Phone: {customer.Phone}</Text>}
          {customer.Email && <Text>Email: {customer.Email}</Text>}
        </View>
      )}
    </>
  );
};

export default SingleCustomer;
