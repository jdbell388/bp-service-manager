import React, { useState, useEffect } from 'react';
import { View, Button } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { getAllOrders } from '../firebase/workorders';
import OrderList from '../components/workorders/OrderList';
import Logout from '../components/auth/Logout';

const WorkOrders = (props: any) => {
  const [orders, setOrders] = useState([]);
  const { navigation } = props;
  const getInitialProps = async () => {
    const results = await getAllOrders();
    setOrders(results);
  };

  useFocusEffect(() => {
    getInitialProps();
  });

  useEffect(() => {
    getInitialProps();
  }, []);
  return (
    <View>
      {orders && (
        <>
          {orders.length > 0 && (
            <OrderList orders={orders} navigation={navigation} />
          )}
        </>
      )}
      <Logout navigation={navigation} />
    </View>
  );
};

export default WorkOrders;
