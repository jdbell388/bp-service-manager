import React, { useState, useEffect } from 'react';
import { View, Text } from 'react-native';

import OrderForm from '../components/workorders/OrderForm';
import { updateOrder, getSingleOrder } from '../firebase/workorders';

const EditOrder = (props: any) => {
  const { route } = props;
  const [order, setOrder] = useState();
  const receivedOrder = route.params.order;

  // const getOrder = async () => {
  //   const result = await getSingleOrder(id);
  //   await setOrder(result);
  //   return true;
  // };

  const submitForm = async (
    Title: any,
    Description: any,
    Assignee: any,
    Client: any,
    tags: any
  ) => {
    await updateOrder({ Title, Description, Assignee, Client, tags, id });
    alert('Updated');
    navigation.navigate('WorkOrders');
  };

  useEffect(() => {
    // getOrder();
    setOrder(receivedOrder);
  }, []);
  return (
    <View>
      <Text>Edit Work Order</Text>
      {order && <OrderForm submit={submitForm} order={order} />}
    </View>
  );
};

export default EditOrder;
