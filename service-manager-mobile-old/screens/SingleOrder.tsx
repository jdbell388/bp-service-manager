import React, { useState, useEffect } from 'react';
import { View, SafeAreaView } from 'react-native';
import { Button, Text, Card, Header, Icon } from 'react-native-elements';

import { deleteOrder, markComplete } from '../firebase/workorders';
import { getSingleCustomer } from '../firebase/customers';

const SingleOrder = ({ navigation, route }: any) => {
  // const [order, setOrder] = useState();
  const [customer, setCustomer] = useState();
  const { order } = route.params;

  const removeOrder = async () => {
    await deleteOrder(order.id);
    alert('order Deleted');
    navigation.navigate('WorkOrders');
  };

  const init = async () => {
    // const result = await getSingleOrder(id);
    if (order && order.Client) {
      const cust = await getSingleCustomer(order.Client);
      await setCustomer(cust);
    }
    // await setOrder(result);
  };

  useEffect(() => {
    init();
  }, []);
  return (
    <SafeAreaView>
      {order && (
        <Card>
          {order.Title && <Text>{order.Title}</Text>}
          {order.Assignee && <Text>{`Assignee: ${order.Assignee}`}</Text>}
          {order.Status && (
            <Text>
              Status:
              {order.Status}
            </Text>
          )}
          {customer && (
            <Text>
              Client:
              {customer.FirstName}
              {customer.LastName}
            </Text>
          )}
          {order.Description && (
            <Text>
              Description:
              {order.Description}
            </Text>
          )}
          {order.tags && (
            <Text>
              Tags:
              {order.tags.map((tag: string) => `${tag}, `)}
            </Text>
          )}
          <Button onPress={() => markComplete(order.id)}>
            <Icon name="checkmark-circle-outline" />
            <Text>Complete</Text>
          </Button>
          <Button
            onPress={() =>
              navigation.navigate('EditWorkorder', {
                order
              })
            }
          >
            <Text>Edit</Text>
          </Button>
          <Button onPress={() => removeOrder()}>
            <Icon name="trash" />
            <Text>Delete</Text>
          </Button>
        </Card>
      )}
    </SafeAreaView>
  );
};

export default SingleOrder;
