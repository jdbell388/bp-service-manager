import React from 'react';
import { View, Text, StyleSheet, Image, ScrollView } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { LinearGradient } from 'expo-linear-gradient';
import Logout from '../components/auth/Logout';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    paddingTop: 100,
    flex: 1
  },
  title: {
    fontSize: 26,
    fontWeight: '600'
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 20,
    justifyContent: 'space-between'
  },
  scrollContainer: {
    paddingTop: 60,
    paddingLeft: 20,
    paddingRight: 20,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  largeButtonContainer: {
    width: '100%',
    height: '100%',
    padding: 0,
    margin: 0
  },
  largeButton: {
    backgroundColor: 'transparent',
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'flex-start',
    padding: 0,
    margin: 0
  },
  linearGradientButton: {
    width: '48%',
    marginBottom: 20,
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    justifyContent: 'space-between',
    height: 150,
    borderRadius: 20,
    display: 'flex',
    flexDirection: 'row'
  },
  buttonTitleStyle: {
    fontWeight: 'bold',
    textAlign: 'left',
    fontSize: 26,
    width: '80%',
    color: '#fff'
  },
  icon: {
    width: '100%',
    color: '#fff'
  }
});

const Dashboard = ({ navigation }: any) => {
  return (
    <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Image
          source={require('../assets/blue-pools-logo.jpg')}
          style={{ width: 100, height: 200 }}
        />
        <Text style={styles.title}>Service Manager</Text>
      </View>
      <ScrollView>
        <View style={styles.scrollContainer}>
          <LinearGradient
            colors={['#0516A2', '#5F6AC9']}
            start={[0.0, 0.0]}
            end={[1.0, 1.0]}
            style={styles.linearGradientButton}
          >
            <Button
              onPress={() => navigation.navigate('WorkOrders')}
              icon={
                <Icon
                  name="ios-add"
                  type="ionicon"
                  size={50}
                  iconStyle={styles.icon}
                />
              }
              iconRight
              title="New Work Order"
              buttonStyle={styles.largeButton}
              titleStyle={styles.buttonTitleStyle}
              containerStyle={styles.largeButtonContainer}
            />
          </LinearGradient>
          <LinearGradient
            colors={['#0516A2', '#5F6AC9']}
            start={[0.0, 0.0]}
            end={[1.0, 1.0]}
            style={styles.linearGradientButton}
          >
            <Button
              onPress={() => navigation.navigate('AddReport')}
              icon={
                <Icon
                  name="ios-add"
                  type="ionicon"
                  size={50}
                  iconStyle={styles.icon}
                />
              }
              iconRight
              title="New Report"
              buttonStyle={styles.largeButton}
              titleStyle={styles.buttonTitleStyle}
            />
          </LinearGradient>
          <LinearGradient
            colors={['#0516A2', '#5F6AC9']}
            start={[0.0, 0.0]}
            end={[1.0, 1.0]}
            style={styles.linearGradientButton}
          >
            <Button
              onPress={() => navigation.navigate('WorkOrders')}
              title="My Work Orders"
              buttonStyle={styles.largeButton}
              titleStyle={styles.buttonTitleStyle}
            />
          </LinearGradient>
          <LinearGradient
            colors={['#0516A2', '#5F6AC9']}
            start={[0.0, 0.0]}
            end={[1.0, 1.0]}
            style={styles.linearGradientButton}
          >
            <Button
              onPress={() => navigation.navigate('Reporting')}
              title="My Route"
              buttonStyle={styles.largeButton}
              titleStyle={styles.buttonTitleStyle}
            />
          </LinearGradient>
          <Logout navigation={navigation} />
        </View>
      </ScrollView>
    </View>
  );
};

export default Dashboard;
