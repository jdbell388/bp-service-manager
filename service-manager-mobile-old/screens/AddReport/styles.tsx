import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    paddingBottom: 30
  },
  headerText: {
    fontSize: 30,
    margin: 20,
    textAlign: 'center'
  },
  textInputContainer: {
    marginBottom: 10,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#ccd1d1'
  },
  textInput: {
    fontSize: 18,
    padding: 10,
    fontWeight: '500'
  },
  label: {
    marginTop: 10,
    marginBottom: 5,
    fontSize: 18,
    paddingLeft: 0,
    fontWeight: '600',
    color: 'gray'
  },
  pickerButtonStyle: {
    padding: 10
  },
  pickerButtonContainerStyle: {
    marginLeft: 10,
    marginRight: 10
  },
  error: {
    color: 'red',
    fontWeight: 'bold',
    paddingLeft: 10,
    paddingBottom: 10,
    fontSize: 14
  }
});

export const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    paddingVertical: 10,
    paddingHorizontal: 10,
    fontSize: 18,
    fontWeight: '500',
    borderWidth: 1,
    borderRadius: 4,
    borderColor: '#ccd1d1',
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    paddingRight: 30 // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    marginBottom: 10,
    borderColor: '#ccd1d1',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30 // to ensure the text is never behind the icon
  }
});
