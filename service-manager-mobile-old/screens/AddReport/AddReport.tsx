import React, { useState, useEffect } from 'react';
import { Text, View, Platform, ActivityIndicator } from 'react-native';
import { Input, Button } from 'react-native-elements';
import DateTimePicker from '@react-native-community/datetimepicker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import RNPickerSelect from 'react-native-picker-select';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { getRelatedAddresses } from '../../queries/Reports';
import { styles, pickerSelectStyles } from './styles';
import { addReport, ReportInt } from '../../firebase/reports';

const AddReport = () => {
  const now = new Date();
  const initialForm = {
    time: now,
    customer: '',
    address: '',
    chlorine: '',
    chlAdded: '',
    ph: '',
    phAdded: '',
    ta: '',
    taAdded: '',
  };
  const [loading, setLoading] = useState(true);
  const [showTimePicker, setShowTimePicker] = useState(false);
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [Customers, setCustomers] = useState([]);
  const [customerNames, setCustomerNames] = useState([]);

  const setRelatedCustomers = async () => {
    const related = await getRelatedAddresses();
    setLoading(true);
    setCustomers(related);
    if (Customers.length > 0) {
      let rows = [];
      Customers.forEach((single: any) => {
        rows = [...rows, { label: single.name, value: single.name }];
      });
      setCustomerNames(rows);
      setLoading(false);
      return true;
    }
    return true;
  };

  useEffect(() => {
    if (Customers.length == 0) {
      console.log('useEffect');
      setRelatedCustomers();
      setLoading(false);
    }
  }, [Customers]);

  return (
    <KeyboardAwareScrollView
      style={{
        flex: 1,
        backgroundColor: '#fff',
        marginBottom: 80,
        paddingLeft: 10,
        paddingRight: 10,
      }}
    >
      <Text style={styles.headerText}>New Report</Text>
      {/* <View>
        <Text style={{ ...styles.label, paddingLeft: 10 }}>Day</Text>
        <Button
          onPress={() =>
            showDatePicker ? setShowDatePicker(false) : setShowDatePicker(true)
          }
          title={form.day.toLocaleDateString()}
          type="outline"
          buttonStyle={styles.pickerButtonStyle}
        />
        {showDatePicker && (
          <DateTimePicker
            value={form.day}
            mode="date"
            display="default"
            onChange={changeDatePicker}
          />
        )}
      </View> */}
      {loading ? (
        <ActivityIndicator size="large" />
      ) : (
        <Formik
          initialValues={initialForm}
          onSubmit={async (values) => {
            setLoading(true);
            const add = await addReport(values);
            alert(add);
          }}
          validationSchema={Yup.object().shape({
            address: Yup.string().required(),
            customer: Yup.string().required(),
            time: Yup.date().required(),
            chlorine: Yup.string().required(),
            ph: Yup.string().required(),
          })}
        >
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            setFieldValue,
            values,
            touched,
            errors,
          }) => (
            <View>
              <View>
                {customerNames.length > 0 && (
                  <>
                    <Text style={{ ...styles.label, paddingLeft: 10 }}>
                      Customer *
                    </Text>
                    <RNPickerSelect
                      onValueChange={(newCustomer: any) => {
                        Customers.forEach((single: any) => {
                          if (single.name === newCustomer) {
                            setFieldValue('address', single.address);
                            setFieldValue('customer', newCustomer);
                          }
                        });
                      }}
                      items={customerNames}
                      style={pickerSelectStyles}
                    />
                    {touched.customer && errors.customer ? (
                      <Text style={styles.error}>{errors.customer}</Text>
                    ) : null}
                  </>
                )}
              </View>
              <View>
                <Text style={{ ...styles.label, paddingLeft: 10 }}>
                  Address
                </Text>
                <Text style={{ ...styles.textInput, paddingLeft: 10 }}>
                  {values.address}
                </Text>
              </View>
              <View>
                <Text style={{ ...styles.label, paddingLeft: 10 }}>Time</Text>
                <Button
                  onPress={() =>
                    showTimePicker
                      ? setShowTimePicker(false)
                      : setShowTimePicker(true)
                  }
                  title={values.time.toLocaleTimeString()}
                  type="outline"
                  buttonStyle={styles.pickerButtonStyle}
                  containerStyle={styles.pickerButtonContainerStyle}
                />
                {showTimePicker && (
                  <DateTimePicker
                    value={values.time}
                    mode="datetime"
                    display="default"
                    onChange={(event, selectedTime) => {
                      const currentTime = selectedTime || values.time;
                      setShowTimePicker(Platform.OS === 'ios');
                      setFieldValue('time', currentTime);
                    }}
                  />
                )}
              </View>
              <Input
                inputContainerStyle={styles.textInputContainer}
                inputStyle={styles.textInput}
                labelStyle={styles.label}
                keyboardType="numeric"
                label="Chlorine *"
                value={values.chlorine}
                onChangeText={handleChange('chlorine')}
              />
              {touched.chlorine && errors.chlorine ? (
                <Text style={styles.error}>{errors.chlorine}</Text>
              ) : null}
              <Input
                inputContainerStyle={styles.textInputContainer}
                inputStyle={styles.textInput}
                labelStyle={styles.label}
                keyboardType="numeric"
                label="Chl Added"
                value={values.chlAdded}
                onChangeText={handleChange('chlAdded')}
              />
              <Input
                inputContainerStyle={styles.textInputContainer}
                inputStyle={styles.textInput}
                labelStyle={styles.label}
                keyboardType="numeric"
                label="PH *"
                value={values.ph}
                onChangeText={handleChange('ph')}
              />
              {touched.ph && errors.ph ? (
                <Text style={styles.error}>{errors.ph}</Text>
              ) : null}
              <Input
                inputContainerStyle={styles.textInputContainer}
                inputStyle={styles.textInput}
                labelStyle={styles.label}
                keyboardType="numeric"
                label="PH Added"
                value={values.phAdded}
                onChangeText={handleChange('phAdded')}
              />
              <Input
                inputContainerStyle={styles.textInputContainer}
                inputStyle={styles.textInput}
                labelStyle={styles.label}
                keyboardType="numeric"
                label="TA"
                value={values.ta}
                onChangeText={handleChange('ta')}
              />
              <Input
                inputContainerStyle={styles.textInputContainer}
                inputStyle={styles.textInput}
                labelStyle={styles.label}
                keyboardType="numeric"
                label="TA Added"
                value={values.taAdded}
                onChangeText={handleChange('taAdded')}
              />
              <Button type="solid" onPress={handleSubmit} title="Submit" />
            </View>
          )}
        </Formik>
      )}
    </KeyboardAwareScrollView>
  );
};

export default AddReport;
