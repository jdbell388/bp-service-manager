import { addNewObj } from './functions';

export interface ReportInt {
  time: Date;
  chlorine: string;
  chlAdded: string;
  ph: string;
  phAdded: string;
  ta: string;
  taAdded: string;
  calcium?: string;
  calciumAdded?: string;
  cya?: string;
  cyaAdded?: string;
  miscAdd?: string;
  customer: string;
  address: string;
}

export const addReport = async (data: ReportInt) => {
  const add = await addNewObj(data, 'reports');
  return add;
};
