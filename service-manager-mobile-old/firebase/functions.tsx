import firebase from './config';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/storage';

export const addNewObj = async (obj: any, collection: string) => {
  const db = firebase.firestore();
  try {
    await db.collection(collection).add(obj);
    return `New ${collection} added successfully!`;
  } catch (e) {
    console.log('Error: ', e);
    return `Error: ${e}`;
  }
};

export const getAllObj = async (collection: string) => {
  const db = firebase.firestore();
  try {
    const objList = await db.collection(collection).get();
    const results: {}[] = [];
    objList.forEach(doc => {
      results.push({ id: doc.id, data: doc.data() });
    });
    return results;
  } catch (e) {
    console.log(`Error getting ${collection}: `, e);
  }
  return [];
};

export const getSingleObj = async (collection: any, id: any) => {
  const db = firebase.firestore();
  try {
    const objList = await db
      .collection(collection)
      .doc(id)
      .get();
    return objList.data();
  } catch (e) {
    console.log(`Error getting ${collection} id: ${id}: `, e);
  }
  return {};
};

export const deleteObj = async (collection: string, id: string) => {
  const db = firebase.firestore();
  try {
    await db
      .collection(collection)
      .doc(id)
      .delete();
    return 200;
  } catch (e) {
    console.log(`Error deleting ${collection}: id: ${id}: `, e);
  }
  return [];
};

export const updateObj = async (
  collection: string,
  id: string,
  updated: any
) => {
  const db = firebase.firestore();

  try {
    const objRef = await db.collection(collection).doc(id);
    const updatedObject = await objRef.set(updated);
    return updatedObject;
  } catch (e) {
    console.log(`Error updating ${collection} id: ${id}: `, e);
  }
  return {};
};
