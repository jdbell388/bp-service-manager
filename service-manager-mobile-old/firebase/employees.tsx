import firebase from './config';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/storage';

export interface EmployeeInt {
  id?: string;
  FirstName?: string;
  LastName?: string;
  Address?: string;
  Email?: string;
  Employee?: string;
  Phone?: string;
}

export const addEmployee = async ({
  FirstName,
  LastName,
  Address,
  Email,
  Phone
}: any) => {
  const db = firebase.firestore();
  try {
    const addDoc = await db
      .collection('employees')
      .add({FirstName, LastName, Address, Email, Phone});
    return addDoc;
  } catch (e) {
    console.log('Error: ', e);
  }
  return {};
};

export const getAllEmployees = async () => {
  const db = firebase.firestore();
  try {
    const employeeList = await db.collection('employees').get();
    const results: {}[] = [];
    employeeList.forEach(doc => {
      results.push({id: doc.id, data: doc.data()});
    });
    return results;
  } catch (e) {
    console.log('Error getting employees: ', e);
  }
  return [];
};

export const getSingleEmployee = async (id: any) => {
  const db = firebase.firestore();
  try {
    const employeeList = await db
      .collection('employees')
      .doc(id)
      .get();
    return employeeList.data();
  } catch (e) {
    console.log('Error getting employee: ', e);
  }
  return {};
};

export const deleteEmployee = async (id: any) => {
  const db = firebase.firestore();
  try {
    await db
      .collection('employees')
      .doc(id)
      .delete();
    return 200;
  } catch (e) {
    console.log('Error deleting employee: ', e);
  }
  return [];
};

export const updateEmployee = async ({
  FirstName,
  LastName,
  Address,
  Email,
  Phone,
  id
}: any) => {
  const db = firebase.firestore();

  try {
    const employeeRef = await db.collection('employees').doc(id);
    const updateDoc = await employeeRef.update({
      FirstName,
      LastName,
      Address,
      Email,
      Phone
    });
    return updateDoc;
  } catch (e) {
    console.log('Error: ', e);
  }
  return {};
};
