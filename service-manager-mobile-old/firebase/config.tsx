import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/storage';

// Initialize Firebase
const config = {
  apiKey: 'AIzaSyBNWcLFVmVagB1VuM4Npkspph1eIS9LrNQ',
  authDomain: 'bluepools-a49a9.firebaseapp.com',
  databaseURL: 'https://bluepools-a49a9.firebaseio.com',
  projectId: 'bluepools-a49a9',
  storageBucket: 'bluepools-a49a9.appspot.com',
  messagingSenderId: '1257732943',
  appId: '1:1257732943:web:e463fc0b6c0116f0'
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

export default firebase;
