import firebase from './config';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/storage';

interface CustomerEmployee {
  Id: string;
  Name: string;
}

interface OrderAcct {
  Id: string;
  Name: string;
}

export interface OrderInt {
  Id?: string;
  Title?: string;
  Description?: string;
  Employee?: CustomerEmployee;
  Account?: OrderAcct;
  Status?: string;
  Tags?: [];
}

export const addOrder = async ({
  Title,
  Description,
  Employee,
  Account
}: OrderInt) => {
  const db = firebase.firestore();
  try {
    const addDoc = await db.collection('orders').add({
      Title,
      Description,
      Employee,
      Account,
      Status: 'In Progress',
      Comments: ''
    });
    return addDoc;
  } catch (e) {
    console.error('Error: ', e);
  }
  return {};
};

export const getAllOrders = async () => {
  const db = firebase.firestore();
  try {
    const orderList = await db.collection('orders').get();
    const results: OrderInt[] = [];
    orderList.forEach(doc => {
      const data = doc.data();
      results.push({ Id: doc.id, ...data });
    });
    return results;
  } catch (e) {
    console.log('Error getting orders: ', e);
  }
  return [];
};

export const getSingleOrder = async (id: string) => {
  const db = firebase.firestore();
  try {
    const orderList = await db
      .collection('orders')
      .doc(id)
      .get();

    const orderListData: OrderInt | undefined = orderList.data();
    return orderListData;
  } catch (e) {
    console.error('Error getting order: ', e);
  }
  return {};
};

export const deleteOrder = async (id: string) => {
  const db = firebase.firestore();
  try {
    await db
      .collection('orders')
      .doc(id)
      .delete();
    return 200;
  } catch (e) {
    console.error('Error deleting order: ', e);
  }
  return [];
};

export const updateOrder = async ({
  Title,
  Description,
  Employee,
  Account,
  Tags,
  Id
}: OrderInt) => {
  const db = firebase.firestore();

  try {
    const orderRef = db.collection('orders').doc(Id);
    const updateDoc = await orderRef.update({
      Title,
      Description,
      Employee,
      Account,
      Tags,
      Status: 'In Progress',
      Comments: ''
    });
    return updateDoc;
  } catch (e) {
    console.error('Error: ', e);
  }
  return {};
};

export const markComplete = async (id: string) => {
  const db = firebase.firestore();

  try {
    const orderRef = db.collection('orders').doc(id);
    const updateDoc = await orderRef.update({
      Status: 'Complete'
    });
    return updateDoc;
  } catch (e) {
    console.error('Error: ', e);
  }
  return {};
};
