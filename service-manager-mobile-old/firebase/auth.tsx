import React, { useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import cookie from "js-cookie";
import firebase from "./config";

export const signin = (email: string, password: string) => {
  firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then(u => {
      cookie.set("token", JSON.stringify(u.user), { expires: 1 });
      // Router.push("/workorders");
    })
    .catch((error: any) => {
      // Handle Errors here.
      const errorCode = error.code;
      const errorMessage = error.message;
      console.log("errorCode", errorCode, "errorMessage", errorMessage);
    });
};

export const auth = (ctx: any) => {
  const token = cookie.get("token");

  // If there's no token, it means the user is not logged in.
  if (!token) {
    if (typeof window === "undefined") {
      ctx.res.writeHead(302, { Location: "/login" });
      ctx.res.end();
    } else {
      // Router.push("/login");
    }
  }

  return token;
};

export const signOut = () => {
  cookie.remove("token");
  firebase
    .auth()
    .signOut()
    .then(() => {
      // Sign-out successful.
      // Router.push("/login");
    })
    .catch((error: any) => {
      // An error happened.
      console.log("error signing out: ", error);
    });
};

export const withAuthSync = (WrappedComponent: any) => {
  const Wrapper = (props: any) => {
    const syncLogout = (event: any) => {
      if (event.key === "logout") {
        console.log("logged out from storage!");
        // Router.push("/login");
      }
    };

    useEffect(() => {
      window.addEventListener("storage", syncLogout);

      return () => {
        window.removeEventListener("storage", syncLogout);
        window.localStorage.removeItem("logout");
      };
    }, []);

    return <WrappedComponent {...props} />;
  };

  Wrapper.getInitialProps = async (ctx: any) => {
    const token = auth(ctx);

    const componentProps =
      WrappedComponent.getInitialProps && (await WrappedComponent.getInitialProps(ctx));

    return { ...componentProps, token };
  };

  return Wrapper;
};
