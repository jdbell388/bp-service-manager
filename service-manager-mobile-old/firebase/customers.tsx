import firebase from './config';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/storage';

export interface CustomerInt {
  id?: string;
  FirstName?: string;
  LastName?: string;
  Address?: string;
  Email?: string;
  Employee?: string;
  Phone?: string;
}

export const addCustomer = async ({
  FirstName,
  LastName,
  Address,
  Email,
  Employee,
  Phone
}: any) => {
  const db = firebase.firestore();
  try {
    const addDoc = await db
      .collection('clients')
      .add({ FirstName, LastName, Address, Email, Employee, Phone });
    return addDoc;
  } catch (e) {
    console.log('Error: ', e);
  }
  return {};
};

export const getAllCustomers = async () => {
  const db = firebase.firestore();
  try {
    const customerList = await db.collection('clients').get();
    const results: {}[] = [];
    customerList.forEach(doc => {
      results.push({ id: doc.id, data: doc.data() });
    });
    return results;
  } catch (e) {
    console.log('Error getting customers: ', e);
  }
  return [];
};

export const getSingleCustomer = async (id: any) => {
  const db = firebase.firestore();
  try {
    const customerList = await db
      .collection('clients')
      .doc(id)
      .get();
    return customerList.data();
  } catch (e) {
    console.log('Error getting customer: ', e);
  }
  return {};
};

export const deleteCustomer = async (id: any) => {
  const db = firebase.firestore();
  try {
    await db
      .collection('clients')
      .doc(id)
      .delete();
    return 200;
  } catch (e) {
    console.log('Error deleting customer: ', e);
  }
  return [];
};

export const updateCustomer = async ({
  FirstName,
  LastName,
  Address,
  Email,
  Employee,
  Phone,
  id
}: any) => {
  const db = firebase.firestore();

  try {
    const customerRef = await db.collection('clients').doc(id);
    const updateDoc = await customerRef.update({
      FirstName,
      LastName,
      Address,
      Email,
      Employee,
      Phone
    });
    return updateDoc;
  } catch (e) {
    console.log('Error: ', e);
  }
  return {};
};
