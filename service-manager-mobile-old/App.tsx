import React, { useEffect, useState } from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { AppLoading } from 'expo';
import { ThemeProvider } from 'react-native-elements';
import { decode, encode } from 'base-64';
import Navigation from './navigation/index';

if (!global.btoa) {
  global.btoa = encode;
}

if (!global.atob) {
  global.atob = decode;
}

const theme = {};

export default function App() {
  const [loaded, setLoaded] = useState(false);
  const load = async () => {
    setLoaded(true);
  };
  useEffect(() => {
    load();
  }, []);
  return (
    <NavigationContainer>
      <ThemeProvider theme={theme}>
        {loaded ? <Navigation /> : <AppLoading />}
      </ThemeProvider>
    </NavigationContainer>
  );
}
