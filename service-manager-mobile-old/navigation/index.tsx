import React, { useEffect, useState } from 'react';
import { AsyncStorage, Text } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { Icon, Button } from 'react-native-elements';

import LoginScreen from '../screens/LoginScreen';
import WorkOrders from '../screens/WorkOrders';
import SingleOrder from '../screens/SingleOrder';
import EditOrder from '../screens/EditOrder';
import AddOrder from '../screens/AddOrder';
import SingleCustomer from '../screens/SingleCustomer';
import Dashboard from '../screens/Dashboard';
import Reporting from '../screens/Reporting';
import ReportDay from '../screens/ReportDay';
import AddReport from '../screens/AddReport/AddReport';

const Stack = createStackNavigator();

function Navigation() {
  const [loggedIn, setLoggedIn] = useState(false);
  const bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('userToken');
    setLoggedIn(!!userToken);
  };
  useEffect(() => {
    bootstrapAsync();
  }, []);

  return (
    <Stack.Navigator>
      {loggedIn ? (
        <>
          <Stack.Screen
            name="Dashboard"
            component={Dashboard}
            options={{
              headerShown: false,
              headerStyle: {
                shadowColor: 'transparent'
              }
            }}
          />
          <Stack.Screen name="Reporting" component={Reporting} />
          <Stack.Screen
            name="ReportDay"
            component={ReportDay}
            options={({ route, navigation }) => ({
              title: route.params.title,
              headerRight: () => (
                <Button
                  type="clear"
                  icon={{
                    name: 'ios-add',
                    type: 'ionicon',
                    size: 28
                  }}
                  iconRight
                  title="Add "
                  onPress={() =>
                    navigation.navigate('AddReport', {
                      day: route.params.title
                    })
                  }
                />
              )
            })}
          />
          <Stack.Screen
            name="AddReport"
            component={AddReport}
            options={() => ({
              title: 'Add Report'
            })}
          />
          <Stack.Screen
            name="WorkOrders"
            component={WorkOrders}
            options={({ navigation }) => ({
              headerTitle: 'WorkOrders',
              headerRight: () => (
                <Button
                  onPress={() => navigation.navigate('AddWorkorder')}
                  title="Add"
                />
              )
            })}
          />
          <Stack.Screen name="SingleOrder" component={SingleOrder} />
          <Stack.Screen name="EditWorkorder" component={EditOrder} />
          <Stack.Screen name="AddWorkorder" component={AddOrder} />
          <Stack.Screen name="SingleCustomer" component={SingleCustomer} />
        </>
      ) : (
        <Stack.Screen name="LogIn" component={LoginScreen} />
      )}
    </Stack.Navigator>
  );
}

export default Navigation;
