import React from 'react';
import { AsyncStorage } from 'react-native';
import { Button } from 'react-native-elements';
import firebase from '../../firebase/config';

export default function Logout({ navigation }: any) {
  const handleSubmit = async (e: any) => {
    e.preventDefault();
    try {
      await firebase.auth().signOut();
      await AsyncStorage.clear();
      navigation.navigate('Login');
    } catch (error) {
      alert(error);
    }
  };

  return (
    <Button
      onPress={() => handleSubmit}
      title="Logout"
      containerStyle={{ width: '100%', marginTop: 10, marginBottom: 10 }}
      buttonStyle={{ backgroundColor: '#0516A2', height: 70, borderRadius: 10 }}
      titleStyle={{ fontWeight: 'bold', color: '#fff', fontSize: 24 }}
    />
  );
}
