import React, { useState } from 'react';
import { AsyncStorage, View, Button, TextInput } from 'react-native';
import firebase from '../../firebase/config';

export default function Login({ navigation }: any) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const handleSubmit = async (e: any) => {
    e.preventDefault();
    try {
      const userAuth = await firebase
        .auth()
        .signInWithEmailAndPassword(email, password);
      await AsyncStorage.setItem('userToken', JSON.stringify(userAuth.user));
      navigation.navigate('WorkOrders');
    } catch (e) {
      console.log(e);
      if (e.code === 'auth/invalid-email') {
        alert('You have entered an invalid Email address');
      }
      if (e.code === 'auth/wrong-password') {
        alert('Incorrect email or password. Please try again.');
      }
    }
  };
  return (
    <>
      <View>
        <TextInput
          onChangeText={change => setEmail(change)}
          placeholder="Email"
          value={email}
          textContentType="username"
        />
      </View>
      <View>
        <TextInput
          onChangeText={change => setPassword(change)}
          placeholder="Password"
          value={password}
          textContentType="password"
          secureTextEntry
        />
      </View>
      <View>
        <Button onPress={e => handleSubmit(e)} title="Login" />
      </View>
    </>
  );
}
