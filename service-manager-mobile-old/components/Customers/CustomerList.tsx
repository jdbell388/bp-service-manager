import React, { useState, useEffect } from 'react';
import { View, Text, Button } from 'react-native';
import { getSingleEmployee } from '../../firebase/employees';

const CustomerList = ({ customers, navigation }: any) => {
  const [employees, setEmployees] = useState<any>({});

  useEffect(() => {
    customers.forEach(async (customer: any) => {
      const employee = await getSingleEmployee(customer.data.Employee);
      await setEmployees((oldObj: any) => ({ ...oldObj, [customer.id]: employee }));
    });
  }, [customers]);
  return (
    <View>
      {customers.map((customer: any) => {
        return (
          <>
            <Text key={customer.id}>{customer.data.Title}</Text>
            <Button
              onPress={() => navigation.navigate({
                routeName: 'SingleCustomer',
                params: {
                  id: customer.id
                }
              })}
              title="View"
            />
          </>
        );
      })}
    </View>
  );
};

export default CustomerList;
