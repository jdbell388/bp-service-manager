import React, { useState, useEffect, useReducer } from 'react';
import { View, Text, Button, TextInput } from 'react-native';
import RelatedField from '../form/RelatedField';

// initial state values
const INITIAL_STATE = {
  Title: '',
  Description: '',
  Employee: {},
  Account: {}
};

const init = initialState => initialState;

const formReducer = (state, action) => {
  switch (action.type) {
    case 'UPDATE_FIELD_VALUE':
      return { ...state, [action.payload.field]: action.payload.value };
    default:
      return INITIAL_STATE;
  }
};

const OrderForm = (props: any) => {
  const [tags, setTags] = useState<Array<string>>([]);
  const [singleTag, setSingleTag] = useState<string>('');
  const { submit, order } = props;
  const [state, dispatch] = useReducer(formReducer, INITIAL_STATE, init);

  const updateFieldValue = (field, value) => {
    dispatch({
      type: 'UPDATE_FIELD_VALUE',
      payload: {
        field,
        value
      }
    });
  };

  const addTag = () => {
    tags ? setTags([...tags, singleTag]) : setTags([singleTag]);
    setSingleTag('');
  };

  const removeTag = (tag: string) => {
    console.log(`remove tag ${tag}`);
    const newTags = tags.filter((single: any) => {
      if (single !== tag) {
        return true;
      }
    });
    setTags(newTags);
    return true;
  };

  // useEffect(() => {
  //   if (order) {
  //     setTitle(order.Title);
  //     setDescription(order.Description);
  //     setEmployee(order.Employee);
  //     setCustomer(order.Client);
  //     setTags(order.tags);
  //   }
  // }, []);

  return (
    <View>
      <View>
        <TextInput
          placeholder="Title"
          value={state.Title}
          onChangeText={(text: any) => {
            updateFieldValue('Title', text);
          }}
        />
      </View>
      <View>
        <TextInput
          placeholder="Description"
          value={state.Description}
          onChangeText={(text: any) => {
            updateFieldValue('Description', text);
          }}
        />
      </View>
      <View>
        <RelatedField
          postType="employees"
          onChange={(text: any) => {
            updateFieldValue('Employee', text);
          }}
          initialValue={state.Employee}
        />
      </View>
      <View>
        <RelatedField
          postType="accounts"
          onChange={(text: any) => {
            updateFieldValue('Account', text);
          }}
          initialValue={state.Account}
        />
      </View>
      {/* <View>
        <Text>Tags: </Text>
        {tags &&
          tags.map((tag: string) => {
            return (
              <>
                <Text key={tag}>{tag}</Text>
                <Button onPress={() => removeTag(tag)} title="X" />
              </>
            );
          })}
        <TextInput
          placeholder="Tag"
          value={singleTag}
          onChangeText={(text: string) => {
            setSingleTag(text);
          }}
        />
        <Button onPress={addTag} title="Add" />
      </View>
      <View> */}
      <Button
        onPress={
          () =>
            submit(
              state.Title,
              state.Description,
              state.Employee,
              state.Account
            )
          // alert(JSON.stringify(state))
        }
        title="Submit"
      />
      {/* </View> */}
    </View>
  );
};

export default OrderForm;
