import React from 'react';
import { useRouter } from 'next/router';
import { ArrowBackIcon } from '@chakra-ui/icons';
import { Link } from '@chakra-ui/react';
import { CustomerForm } from '../../components/customers/customerForm';
import { updateCustomer, getSingleCustomer } from '../../firebase/customers';
import { Dashboard } from '../../components/view';

export default function EditCustomer({ customer }: any) {
  const router = useRouter();
  const { id } = router.query;
  if (!id) return;
  const submitForm = async (
    FirstName: any,
    LastName: any,
    Address: any,
    Email: any,
    Employee: any,
    Phone: any
  ) => {
    await updateCustomer({
      FirstName,
      LastName,
      Address,
      Email,
      Employee,
      Phone,
      id,
    });
    alert('Updated');
    router.push(`/customers/customer?id=${id}`);
  };
  console.log('customer', customer);
  return (
    <Dashboard
      title="Edit Customer"
      backButton={(
        <Link href="/customers/">
          <ArrowBackIcon /> Back
        </Link>
      )}
    >
      {customer && <CustomerForm submit={submitForm} customer={customer} />}
    </Dashboard>
  );
}

export async function getServerSideProps(context: any) {
  const { id } = context.query;
  try {
    const customer = await getSingleCustomer(id);
    return { props: { customer } };
  } catch (e) {
    return false;
  }
}
