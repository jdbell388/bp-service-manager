import React from 'react';
import { useRouter } from 'next/router';
import { ArrowBackIcon } from '@chakra-ui/icons';
import { Link } from '@chakra-ui/react';
import { CustomerForm } from '../../components/customers/customerForm';
import { addCustomer } from '../../firebase/customers';
import { Dashboard } from '../../components/view';

export default function NewCustomer() {
  const router = useRouter();
  const submitForm = async (
    FirstName: any,
    LastName: any,
    Address: any,
    Email: any,
    Employee: any,
    Phone: any
  ) => {
    await addCustomer({
      FirstName,
      LastName,
      Address,
      Email,
      Employee,
      Phone,
    });
    alert('added');
    router.push('/customers');
  };
  return (
    <Dashboard
      title="New Customer"
      backButton={(
        <Link href="/customers/">
          <ArrowBackIcon />
{' '}
Back
</Link>
      )}
    >
      <CustomerForm submit={submitForm} />
    </Dashboard>
  );
}
