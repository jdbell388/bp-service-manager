import React from 'react';
import { useRouter } from 'next/router';
import { Link } from '@chakra-ui/react';
import { ArrowBackIcon } from '@chakra-ui/icons';
import { CustomerSingle } from '../../components/customers/index';
import { getSingleCustomer } from '../../firebase/customers';
import { Dashboard } from '../../components/view';

export default function Customer({ results }: any) {
  const router = useRouter();
  const { id } = router.query;
  return (
    <Dashboard
      title={`${results.FirstName} 
    ${results.LastName}`}
      backButton={
        <Link href="/customers/">
          <ArrowBackIcon />
{' '}
Back
</Link>
      }
    >
      <CustomerSingle customer={results} id={id} title={false} />
    </Dashboard>
  );
}

export async function getServerSideProps(context: any) {
  const { id } = context.query;
  try {
    const results = await getSingleCustomer(id);
    return { props: { results } };
  } catch (e) {
    return false;
  }
}
