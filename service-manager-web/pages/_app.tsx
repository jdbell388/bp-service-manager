import '../styles/globals.css';
import { ChakraProvider, extendTheme } from '@chakra-ui/react';
import { AuthProvider } from '../context/authContext';

// 2. Extend the theme to include custom colors, fonts, etc
const colors = {
  brand: {
    vd: '#2874a6',
    d: '#2874a6',
    m: '#3498db ',
    l: '#85c1e9',
    tomato: 'tomato',
  },
  components: {
    IconButton: {
      colorScheme: {
        tomato: {
          color: 'tomato',
        },
      },
    },
  },
};

const theme = extendTheme({ colors });

function MyApp({ Component, pageProps }: any) {
  return (
    <AuthProvider>
      <ChakraProvider theme={theme}>
        <Component {...pageProps} />
      </ChakraProvider>
    </AuthProvider>
  );
}

export default MyApp;
