import React from 'react';
import { Link } from '@chakra-ui/react';
import { AddIcon } from '@chakra-ui/icons';
import { EmployeeList } from '../components/employees/index';
import { getAllEmployees } from '../firebase/employees';
import { Dashboard } from '../components/view';
import { withAuthSync, protectPage } from '../firebase/auth';

function employees({ results }: any) {
  return (
    <Dashboard title="Employees">
      {results && <EmployeeList employees={results} />}
      <Link
        href="/employees/new"
        p="10px"
        rounded="md"
        bg="LightGreen"
        mt="20px"
        boxShadow="md"
        display="inline-block"
      >
        <AddIcon boxSize="3" /> New Employee
      </Link>
    </Dashboard>
  );
}

export async function getServerSideProps(context: any) {
  try {
    await protectPage(context);
    const results = await getAllEmployees();
    return { props: { results } };
  } catch (e) {
    return false;
  }
}

export default withAuthSync(employees)