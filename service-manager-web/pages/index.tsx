import React from 'react';
import { Dashboard } from '../components/view';
import { withAuthSync, checkAuthentication } from '../firebase/auth';
import { getAllReports } from '../firebase/reports'
import { ReportList } from '../components/reports'

function Home({ results }: any) {
  return (
    <Dashboard title="Reports">
      {results && <ReportList reports={results} />}
    </Dashboard>
  );
}
export default withAuthSync(Home)

export async function getServerSideProps(context: any) {
  const results = await getAllReports();
  return { props: { results } };
  try {
    return { props: {} }
  } catch (e) {
    return false;
  }
}