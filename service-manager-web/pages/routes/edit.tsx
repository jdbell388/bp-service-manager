import React from 'react';
import { useRouter } from 'next/router';
import { ArrowBackIcon } from '@chakra-ui/icons';
import { Link } from '@chakra-ui/react';
import { RouteForm } from '../../components/routes';
import { updateRoute, getSingleRoute } from '../../firebase/routes';
import { Dashboard } from '../../components/view';

export default function EditRoute({ route }: any) {
  const router = useRouter();
  const { id } = router.query;

  const submitForm = async (values: any) => {
    const routeObj = {
      day: values.day,
      route: values.stops,
      employeeName: route.employeeName,
      employeeId: route.employeeId,
      id
    };
    console.log("routeobj", routeObj);
    await updateRoute(routeObj);
    alert('Route Added');
    router.push(`/routes`);
  };
  return (
    <Dashboard
      title="Edit Route"
      backButton={
        <Link href={`/routes`}>
          <ArrowBackIcon />
{' '}
Back
</Link>
      }
    >
      {route && <RouteForm submitRouteForm={submitForm} editRoute={route} />}
    </Dashboard>
  );
}

export async function getServerSideProps(context: any) {
  const { id } = context.query;
  try {
    const route = await getSingleRoute(id);
    return { props: { route } };
  } catch (e) {
    return false;
  }
}
