import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { Button, Heading, Link } from '@chakra-ui/react';
import { AddIcon, ArrowBackIcon } from '@chakra-ui/icons';
import { useEmployeeRoutes } from '../../components/routes/Route-hooks';
import { RouteInt, getAllEmployeeRoutes } from '../../firebase/routes';
import { getSingleEmployee } from '../../firebase/employees';
import { EmployeesRoutesByDay } from '../../components/routes';
import { Dashboard } from '../../components/view';

interface Week {
  sunday: number;
  monday: number;
  tuesday: number;
  wednesday: number;
  thursday: number;
  friday: number;
  saturday: number;
}

interface Day {
  day: string;
}


export default function EmployeesRoutes({ routes, employee, id }: any) {
  const router = useRouter();
  const [sortedRoutes, setSortedRoutes] = useState(routes);
  const [day, setDay] = useState<any>();
  const [activeRoute, setActiveRoute] = useState({});

  const sortByDay = (a: Day, b: Day) => {
    const day1 = a.day.toLowerCase();
    const day2 = b.day.toLowerCase();
    const sorter: {[key: string]: number} = {
      'sunday': 0,
      'monday': 1,
      'tuesday': 2,
      'wednesday': 3,
      'thursday': 4,
      'friday': 5,
      'saturday': 6,
    };
    return sorter[day1] - sorter[day2];
  };


  useEffect(() => {
    console.log("routes", routes);
    const newRoutes = routes.sort(sortByDay);
    setSortedRoutes(newRoutes);
  }, [routes]);

  const title = day
    ? `${employee.FirstName} ${employee.LastName}'s ${day} Route`
    : `${employee.FirstName} ${employee.LastName}'s Routes`;
  return (
    <Dashboard
      title={title}
      backButton={(
        <Link href="/routes/">
          <ArrowBackIcon /> Back
        </Link>
      )}
    >
      {day ? (
        <EmployeesRoutesByDay day={day} id={id} setDay={setDay} route={activeRoute} />
      ) : (
        <>
          {routes && (
            <>
              {routes.length > 0 && (
                <div>
                  <Heading as="h3" size="md" mb="20px">
                    Select A Day
                  </Heading>
                  <ul>
                    {sortedRoutes?.length &&
                      sortedRoutes.map((single: RouteInt, index: number) => (
                        <Button
                          key={single.day}
                          type="button"
                          bg="brand.d"
                          color="white"
                          mr="10px"
                          _hover={{ background: '#333' }}
                          onClick={() => {
                            setDay(single.day)
                            setActiveRoute(single)
                          }}
                        >
                          {single.day}
                        </Button>
                      ))}
                  </ul>
                </div>
              )}

              <div>
                <Button
                  p="10px"
                  rounded="md"
                  bg="LightGreen"
                  mt="20px"
                  boxShadow="md"
                  type="button"
                  onClick={() => {
                    router.push(`/routes/new?id=${id}`);
                  }}
                >
                  <AddIcon boxSize="3" /> Add Route
                </Button>
              </div>
            </>
          )}
        </>
      )}
    </Dashboard>
  );
}

export async function getServerSideProps(context: any) {
  const { id } = context.query;
  try {
    const routes = await getAllEmployeeRoutes(id);
    const employee = await getSingleEmployee(id);
    return { props: { routes, employee, id } };
  } catch (e) {
    return false;
  }
}
