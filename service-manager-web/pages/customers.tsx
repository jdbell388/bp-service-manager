import React from 'react';
import { Link } from '@chakra-ui/react';
import { AddIcon } from '@chakra-ui/icons';
import { CustomerList } from '../components/customers/index';
import { getAllCustomers } from '../firebase/customers';
import { Dashboard } from '../components/view';
import { withAuthSync, protectPage } from '../firebase/auth';

function customers({ results, token }: any) {
  return (
    <Dashboard title="Customers">
      {results && <CustomerList customers={results} />}
      <Link
        href="/customers/new"
        p="10px"
        rounded="md"
        bg="LightGreen"
        mt="20px"
        boxShadow="md"
        display="inline-block"
      >
        <AddIcon boxSize="3" /> New Customer
      </Link>
    </Dashboard>
  );
}

export async function getServerSideProps(context: any) {
  
  try {
    const results = await getAllCustomers();
    return { props: { results } };
  } catch (e) {
    return false;
  }
}

export default withAuthSync(customers);