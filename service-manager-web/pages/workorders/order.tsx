import React from 'react';
import { Link } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { ArrowBackIcon } from '@chakra-ui/icons';
import { OrderCard } from '../../components/workorders';
import { getSingleOrder, OrderInt } from '../../firebase/workorders';
import { Dashboard } from '../../components/view';

function SingleOrder(order: OrderInt) {
  const router = useRouter();
  return (
    <Dashboard
      title={order.Title}
      backButton={
        <Link href="/workorders/">
          <ArrowBackIcon /> Back
        </Link>
      }
    >
      {order && <OrderCard order={order} />}
    </Dashboard>
  );
}

export default SingleOrder;

export async function getServerSideProps(context: any) {
  const { id } = context.query;
  try {
    const order = await getSingleOrder(id);
    return {
      props: {
        ...order,
        id,
      },
    };
  } catch (e) {
    return false;
  }
}
