import React from 'react';
import { Link } from '@chakra-ui/react';
import { AddIcon } from '@chakra-ui/icons';
import { getAllOrders } from '../firebase/workorders';
import { OrderList } from '../components/workorders';
import { Dashboard } from '../components/view';
import { withAuthSync, protectPage } from '../firebase/auth';

function workorders({ orders }: any) {
  return (
    <Dashboard title="Work Orders">
      {orders.length > 0 && <OrderList orders={orders} />}
      <Link
        href="/workorders/new"
        p="10px"
        rounded="md"
        bg="LightGreen"
        mt="20px"
        boxShadow="md"
        display="inline-block"
      >
        <AddIcon boxSize="3" />
{' '}
New Work Order
</Link>
    </Dashboard>
  );
}

export async function getServerSideProps(context: any) {
  try {
    await protectPage(context)
    const orders = await getAllOrders();
    return { props: { orders } };
  } catch (e) {
    return false;
  }
}

export default withAuthSync(workorders)