import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { getAuth } from 'firebase/auth';

// Initialize Firebase
const config = {
  apiKey: process.env.NEXT_PUBLIC_FIREBASE_PUBLIC_API_KEY,
  authDomain: process.env.NEXT_PUBLIC_FIREBASE_AUTH_DOMAIN,
  projectId: process.env.NEXT_PUBLIC_FIREBASE_PROJECT_ID,
  databaseURL: "https://bluepools-a49a9.firebaseio.com",
  storageBucket: "bluepools-a49a9.appspot.com",
  messagingSenderId: "1257732943",
  appId: "1:1257732943:web:e463fc0b6c0116f0"
};

export const app = initializeApp(config);
export const database = getFirestore(app);
export const auth = getAuth(app);
