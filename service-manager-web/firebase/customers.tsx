import { collection, doc, setDoc, getDocs, getDoc, deleteDoc } from "firebase/firestore"; 
import { database } from '../firebase/config';
interface CustomerEmployee {
  id: string;
  name: string;
}
export interface CustomerInt {
  Address?: string;
  Email?: string;
  Employee?: CustomerEmployee;
  FirstName?: string;
  LastName?: string;
  Phone?: string;
  id?: any;
}



export const addCustomer = async ({
  FirstName,
  LastName,
  Address,
  Email,
  Employee,
  Phone,
}: CustomerInt) => {
  const accountsRef = collection(database, 'accounts');
  try {
    const addDoc = await setDoc(doc(accountsRef), { FirstName, LastName, Address, Email, Employee, Phone });
    return addDoc;
  } catch (e) {
    console.error('Error: ', e);
  }
  return {};
};

export const getAllCustomers = async () => {

  try {
    const customerList = await getDocs(collection(database, 'accounts'));
    const results: CustomerInt[] = [];
    customerList.forEach((doc) => {
      const data = doc.data();
      results.push({ id: doc.id, ...data });
    });
    return results;
  } catch (e) {
    console.error('Error getting customers: ', e);
  }
  return [];
};

export const getSingleCustomer = async (id: string) => {
  const docRef = doc(database, "accounts", id);
  try {
    const customerList = await getDoc(docRef);

    const customerListData: CustomerInt | undefined = customerList.data();
    return customerListData;
  } catch (e) {
    console.error('Error getting customer: ', e);
  }
  return {};
};

export const deleteCustomer = async (id: string) => {

  try {
    await deleteDoc(doc(database, 'accounts', id));
    return 200;
  } catch (e) {
    console.error('Error deleting customer: ', e);
  }
  return [];
};

export const updateCustomer = async ({
  FirstName,
  LastName,
  Address,
  Email,
  Employee,
  Phone,
  id,
}: CustomerInt) => {


  try {
    const updateDoc = await setDoc(doc(database, 'accounts', id), {
      FirstName,
      LastName,
      Address,
      Email,
      Employee,
      Phone,
    });
    return updateDoc;
  } catch (e) {
    console.error('Error: ', e);
  }
  return {};
};
