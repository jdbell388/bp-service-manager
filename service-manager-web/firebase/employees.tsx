import { collection, doc, setDoc, getDocs, getDoc, deleteDoc } from "firebase/firestore"; 
import { database } from '../firebase/config';

export interface EmployeeInt {
  id?: any;
  FirstName?: string;
  LastName?: string;
  Address?: string;
  Email?: string;
  Phone?: string;
}

export const addEmployee = async ({
  FirstName,
  LastName,
  Address,
  Email,
  Phone,
}: EmployeeInt) => {
  const employeeRef = collection(database, 'employees');
  try {
    const addDoc = await setDoc(doc(employeeRef), { FirstName, LastName, Address, Email, Phone });
    return addDoc;
  } catch (e) {
    console.error('Error: ', e);
  }
  return {};
};

export const getAllEmployees = async () => {
  try {
    const employeeList = await getDocs(collection(database, 'employees'));
    const results: EmployeeInt[] = [];
    employeeList.forEach((doc) => {
      const data = doc.data();
      results.push({ id: doc.id, ...data });
    });
    return results;
  } catch (e) {
    console.error('Error getting employees: ', e);
  }
  return [];
};

export const getSingleEmployee = async (id: string) => {
  const docRef = doc(database, "employees", id);
  try {
    const employeeList = await getDoc(docRef);

    const employeeListData: EmployeeInt | undefined = employeeList.data();
    return employeeListData;
  } catch (e) {
    console.error('Error getting employee: ', e);
  }
  return {};
};

export const deleteEmployee = async (id: string) => {
  try {
    await deleteDoc(doc(database, 'employees', id));
    return 200;
  } catch (e) {
    console.error('Error deleting employee: ', e);
  }
  return [];
};

export const updateEmployee = async ({
  FirstName,
  LastName,
  Address,
  Email,
  Phone,
  id,
}: EmployeeInt) => {

  try {
    const updateDoc = await setDoc(doc(database, 'employees', id), {
      FirstName,
      LastName,
      Address,
      Email,
      Phone,
    });
    return updateDoc;
  } catch (e) {
    console.error('Error: ', e);
  }
  return {};
};
