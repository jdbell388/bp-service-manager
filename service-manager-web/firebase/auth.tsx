import React, { useEffect } from 'react';
import Router from 'next/router';
import nextCookie from 'next-cookies';
import cookie from 'js-cookie';
import { signInWithEmailAndPassword, createUserWithEmailAndPassword, signOut } from "firebase/auth";
import { auth } from './config';
import nookies from 'nookies'

export const login = (email: any, password: any) => {
  signInWithEmailAndPassword(auth, email, password)
  .then(async (userCredential) => {
    const user = userCredential.user;
    if (user) {
      const userToken = await user.getIdToken();
      console.log('idtoken', userToken);
      cookie.set('session', JSON.stringify(userToken), { expires: 1, secure: false, path: '/' });
      Router.push('/');
    }
  })
  .catch((error) => {
    const errorCode = error.code;
    const errorMessage = error.message;
    console.log('errorCode', errorCode, 'errorMessage', errorMessage);
  });
};

export const logout = () => {
  cookie.remove('token');
  signOut(auth)
    .then(() => {
      Router.push('/login');
    })
    .catch((error) => {
      console.log('error signing out: ', error);
    });
};

export const createUser = (email: string, password: string) => {
  createUserWithEmailAndPassword(auth, email, password)
  .then((userCredential) => {
    const user = userCredential.user;
    cookie.set('session', JSON.stringify(user), { expires: 1 });
    Router.push('/');
  })
  .catch((error) => {
    const errorCode = error.code;
    const errorMessage = error.message;
    console.log('errorCode', errorCode, 'errorMessage', errorMessage);
  });
}

export const checkAuthentication = (ctx: any) => {
  const cookies = nookies.get(ctx)
  // const { __session, token } = cookies;

  // console.log('checkingauth', __session);
  // If there's no token, it means the user is not logged in.
  // if (!cookies) {
  //   if (typeof window === 'undefined') {
  //     ctx.res.writeHead(302, { Location: '/login' });
  //     ctx.res.end();
  //   } else {
  //     Router.push('/login');
  //   }
  // }

  return cookies;
};


export const withAuthSync = (WrappedComponent: any) => {
  const Wrapper = (props: any) => {
    const syncLogout = (event: any) => {
      if (event.key === 'logout') {
        console.log('logged out from storage!');
        Router.push('/login');
      }
    };

    useEffect(() => {
      window.addEventListener('storage', syncLogout);

      return () => {
        window.removeEventListener('storage', syncLogout);
        window.localStorage.removeItem('logout');
      };
    }, []);

    return <WrappedComponent {...props} />;
  };

  return Wrapper;
};

export const protectPage = (context: any) => {
  const token = checkAuthentication(context);
  return token;
}