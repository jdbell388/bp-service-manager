/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/_app";
exports.ids = ["pages/_app"];
exports.modules = {

/***/ "./context/authContext.tsx":
/*!*********************************!*\
  !*** ./context/authContext.tsx ***!
  \*********************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"AuthProvider\": () => (/* binding */ AuthProvider),\n/* harmony export */   \"useAuth\": () => (/* binding */ useAuth)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var nookies__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! nookies */ \"nookies\");\n/* harmony import */ var nookies__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(nookies__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _firebase_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../firebase/config */ \"./firebase/config.tsx\");\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_firebase_config__WEBPACK_IMPORTED_MODULE_3__]);\n_firebase_config__WEBPACK_IMPORTED_MODULE_3__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];\n\n\n\n\nconst AuthContext = /*#__PURE__*/ (0,react__WEBPACK_IMPORTED_MODULE_1__.createContext)({\n    user: null\n});\nfunction AuthProvider({ children  }) {\n    const { 0: user , 1: setUser  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(null);\n    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{\n        if (false) {}\n        return _firebase_config__WEBPACK_IMPORTED_MODULE_3__.auth.onIdTokenChanged(async (user)=>{\n            console.log(`token changed!`);\n            if (!user) {\n                console.log(`no token found...`);\n                setUser(null);\n                nookies__WEBPACK_IMPORTED_MODULE_2___default().destroy(null, \"session\");\n                nookies__WEBPACK_IMPORTED_MODULE_2___default().set(null, \"session\", \"\", {\n                    path: \"/\",\n                    secure: false\n                });\n                return;\n            }\n            console.log(`updating token...`);\n            const token = await user.getIdToken();\n            setUser(user);\n            nookies__WEBPACK_IMPORTED_MODULE_2___default().destroy(null, \"session\");\n            nookies__WEBPACK_IMPORTED_MODULE_2___default().set(null, \"session\", token, {\n                path: \"/\",\n                secure: false\n            });\n        });\n    }, []);\n    // force refresh the token every 10 minutes\n    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{\n        const handle = setInterval(async ()=>{\n            console.log(`refreshing token...`);\n            const user = _firebase_config__WEBPACK_IMPORTED_MODULE_3__.auth.currentUser;\n            if (user) await user.getIdToken(true);\n        }, 10 * 60 * 1000);\n        return ()=>clearInterval(handle);\n    }, []);\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(AuthContext.Provider, {\n        value: {\n            user\n        },\n        children: children\n    }, void 0, false, {\n        fileName: \"/Users/joshbell/Personal/bp-service-manager/service-manager-web/context/authContext.tsx\",\n        lineNumber: 45,\n        columnNumber: 5\n    }, this);\n}\nconst useAuth = ()=>{\n    return (0,react__WEBPACK_IMPORTED_MODULE_1__.useContext)(AuthContext);\n};\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9jb250ZXh0L2F1dGhDb250ZXh0LnRzeC5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUE4RTtBQUNoRDtBQUNZO0FBRTFDLE1BQU1PLFdBQVcsaUJBQUdILG9EQUFhLENBQWdCO0lBQy9DSSxJQUFJLEVBQUUsSUFBSTtDQUNYLENBQUM7QUFFSyxTQUFTQyxZQUFZLENBQUMsRUFBRUMsUUFBUSxHQUFPLEVBQUU7SUFDOUMsTUFBTSxLQUFDRixJQUFJLE1BQUVHLE9BQU8sTUFBSVYsK0NBQVEsQ0FBTSxJQUFJLENBQUM7SUFFM0NDLGdEQUFTLENBQUMsSUFBTTtRQUNkLElBQUksS0FBNkIsRUFBRSxFQUVsQztRQUNELE9BQU9JLG1FQUFxQixDQUFDLE9BQU9FLElBQVMsR0FBSztZQUNoRE0sT0FBTyxDQUFDQyxHQUFHLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQzlCLElBQUksQ0FBQ1AsSUFBSSxFQUFFO2dCQUNUTSxPQUFPLENBQUNDLEdBQUcsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztnQkFDakNKLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDZE4sc0RBQWUsQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLENBQUM7Z0JBQ2pDQSxrREFBVyxDQUFDLElBQUksRUFBRSxTQUFTLEVBQUUsRUFBRSxFQUFFO29CQUFDYSxJQUFJLEVBQUUsR0FBRztvQkFBRUMsTUFBTSxFQUFFLEtBQUs7aUJBQUMsQ0FBQyxDQUFDO2dCQUM3RCxPQUFPO2FBQ1I7WUFFREwsT0FBTyxDQUFDQyxHQUFHLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7WUFDakMsTUFBTUssS0FBSyxHQUFHLE1BQU1aLElBQUksQ0FBQ2EsVUFBVSxFQUFFO1lBQ3JDVixPQUFPLENBQUNILElBQUksQ0FBQyxDQUFDO1lBQ2RILHNEQUFlLENBQUMsSUFBSSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1lBQ2pDQSxrREFBVyxDQUFDLElBQUksRUFBRSxTQUFTLEVBQUVlLEtBQUssRUFBRTtnQkFBQ0YsSUFBSSxFQUFFLEdBQUc7Z0JBQUVDLE1BQU0sRUFBRSxLQUFLO2FBQUMsQ0FBQyxDQUFDO1NBQ2pFLENBQUMsQ0FBQztLQUNKLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFFUCwyQ0FBMkM7SUFDM0NqQixnREFBUyxDQUFDLElBQU07UUFDZCxNQUFNb0IsTUFBTSxHQUFHQyxXQUFXLENBQUMsVUFBWTtZQUNyQ1QsT0FBTyxDQUFDQyxHQUFHLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7WUFDbkMsTUFBTVAsSUFBSSxHQUFHRiw4REFBZ0I7WUFDN0IsSUFBSUUsSUFBSSxFQUFFLE1BQU1BLElBQUksQ0FBQ2EsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3ZDLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUM7UUFDbEIsT0FBTyxJQUFNSSxhQUFhLENBQUNILE1BQU0sQ0FBQyxDQUFDO0tBQ3BDLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFFUCxxQkFDRSw4REFBQ2YsV0FBVyxDQUFDbUIsUUFBUTtRQUFDQyxLQUFLLEVBQUU7WUFBRW5CLElBQUk7U0FBRTtrQkFBR0UsUUFBUTs7Ozs7WUFBd0IsQ0FDeEU7Q0FDSDtBQUVNLE1BQU1rQixPQUFPLEdBQUcsSUFBTTtJQUMzQixPQUFPekIsaURBQVUsQ0FBQ0ksV0FBVyxDQUFDLENBQUM7Q0FDaEMsQ0FBQyIsInNvdXJjZXMiOlsid2VicGFjazovL3NlcnZpY2UtbWFuYWdlci13ZWIvLi9jb250ZXh0L2F1dGhDb250ZXh0LnRzeD81NjIzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyB1c2VTdGF0ZSwgdXNlRWZmZWN0LCB1c2VDb250ZXh0LCBjcmVhdGVDb250ZXh0IH0gZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgbm9va2llcyBmcm9tIFwibm9va2llc1wiO1xuaW1wb3J0IHsgYXV0aCB9IGZyb20gXCIuLi9maXJlYmFzZS9jb25maWdcIjtcblxuY29uc3QgQXV0aENvbnRleHQgPSBjcmVhdGVDb250ZXh0PHsgdXNlcjogYW55IH0+KHtcbiAgdXNlcjogbnVsbCxcbn0pO1xuXG5leHBvcnQgZnVuY3Rpb24gQXV0aFByb3ZpZGVyKHsgY2hpbGRyZW4gfTogYW55KSB7XG4gIGNvbnN0IFt1c2VyLCBzZXRVc2VyXSA9IHVzZVN0YXRlPGFueT4obnVsbCk7XG5cbiAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICBpZiAodHlwZW9mIHdpbmRvdyAhPT0gXCJ1bmRlZmluZWRcIikge1xuICAgICAgKHdpbmRvdyBhcyBhbnkpLm5vb2tpZXMgPSBub29raWVzO1xuICAgIH1cbiAgICByZXR1cm4gYXV0aC5vbklkVG9rZW5DaGFuZ2VkKGFzeW5jICh1c2VyOiBhbnkpID0+IHtcbiAgICAgIGNvbnNvbGUubG9nKGB0b2tlbiBjaGFuZ2VkIWApO1xuICAgICAgaWYgKCF1c2VyKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKGBubyB0b2tlbiBmb3VuZC4uLmApO1xuICAgICAgICBzZXRVc2VyKG51bGwpO1xuICAgICAgICBub29raWVzLmRlc3Ryb3kobnVsbCwgXCJzZXNzaW9uXCIpO1xuICAgICAgICBub29raWVzLnNldChudWxsLCBcInNlc3Npb25cIiwgXCJcIiwge3BhdGg6ICcvJywgc2VjdXJlOiBmYWxzZX0pO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIGNvbnNvbGUubG9nKGB1cGRhdGluZyB0b2tlbi4uLmApO1xuICAgICAgY29uc3QgdG9rZW4gPSBhd2FpdCB1c2VyLmdldElkVG9rZW4oKTtcbiAgICAgIHNldFVzZXIodXNlcik7XG4gICAgICBub29raWVzLmRlc3Ryb3kobnVsbCwgXCJzZXNzaW9uXCIpO1xuICAgICAgbm9va2llcy5zZXQobnVsbCwgXCJzZXNzaW9uXCIsIHRva2VuLCB7cGF0aDogJy8nLCBzZWN1cmU6IGZhbHNlfSk7XG4gICAgfSk7XG4gIH0sIFtdKTtcblxuICAvLyBmb3JjZSByZWZyZXNoIHRoZSB0b2tlbiBldmVyeSAxMCBtaW51dGVzXG4gIHVzZUVmZmVjdCgoKSA9PiB7XG4gICAgY29uc3QgaGFuZGxlID0gc2V0SW50ZXJ2YWwoYXN5bmMgKCkgPT4ge1xuICAgICAgY29uc29sZS5sb2coYHJlZnJlc2hpbmcgdG9rZW4uLi5gKTtcbiAgICAgIGNvbnN0IHVzZXIgPSBhdXRoLmN1cnJlbnRVc2VyO1xuICAgICAgaWYgKHVzZXIpIGF3YWl0IHVzZXIuZ2V0SWRUb2tlbih0cnVlKTtcbiAgICB9LCAxMCAqIDYwICogMTAwMCk7XG4gICAgcmV0dXJuICgpID0+IGNsZWFySW50ZXJ2YWwoaGFuZGxlKTtcbiAgfSwgW10pO1xuXG4gIHJldHVybiAoXG4gICAgPEF1dGhDb250ZXh0LlByb3ZpZGVyIHZhbHVlPXt7IHVzZXIgfX0+e2NoaWxkcmVufTwvQXV0aENvbnRleHQuUHJvdmlkZXI+XG4gICk7XG59XG5cbmV4cG9ydCBjb25zdCB1c2VBdXRoID0gKCkgPT4ge1xuICByZXR1cm4gdXNlQ29udGV4dChBdXRoQ29udGV4dCk7XG59OyJdLCJuYW1lcyI6WyJSZWFjdCIsInVzZVN0YXRlIiwidXNlRWZmZWN0IiwidXNlQ29udGV4dCIsImNyZWF0ZUNvbnRleHQiLCJub29raWVzIiwiYXV0aCIsIkF1dGhDb250ZXh0IiwidXNlciIsIkF1dGhQcm92aWRlciIsImNoaWxkcmVuIiwic2V0VXNlciIsIndpbmRvdyIsIm9uSWRUb2tlbkNoYW5nZWQiLCJjb25zb2xlIiwibG9nIiwiZGVzdHJveSIsInNldCIsInBhdGgiLCJzZWN1cmUiLCJ0b2tlbiIsImdldElkVG9rZW4iLCJoYW5kbGUiLCJzZXRJbnRlcnZhbCIsImN1cnJlbnRVc2VyIiwiY2xlYXJJbnRlcnZhbCIsIlByb3ZpZGVyIiwidmFsdWUiLCJ1c2VBdXRoIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./context/authContext.tsx\n");

/***/ }),

/***/ "./firebase/config.tsx":
/*!*****************************!*\
  !*** ./firebase/config.tsx ***!
  \*****************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"app\": () => (/* binding */ app),\n/* harmony export */   \"auth\": () => (/* binding */ auth),\n/* harmony export */   \"database\": () => (/* binding */ database)\n/* harmony export */ });\n/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! firebase/app */ \"firebase/app\");\n/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! firebase/firestore */ \"firebase/firestore\");\n/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/auth */ \"firebase/auth\");\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([firebase_app__WEBPACK_IMPORTED_MODULE_0__, firebase_firestore__WEBPACK_IMPORTED_MODULE_1__, firebase_auth__WEBPACK_IMPORTED_MODULE_2__]);\n([firebase_app__WEBPACK_IMPORTED_MODULE_0__, firebase_firestore__WEBPACK_IMPORTED_MODULE_1__, firebase_auth__WEBPACK_IMPORTED_MODULE_2__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);\n\n\n\n// Initialize Firebase\nconst config = {\n    apiKey: \"AIzaSyBNWcLFVmVagB1VuM4Npkspph1eIS9LrNQ\",\n    authDomain: \"bluepools-a49a9.firebaseapp.com\",\n    projectId: \"bluepools-a49a9\",\n    databaseURL: \"https://bluepools-a49a9.firebaseio.com\",\n    storageBucket: \"bluepools-a49a9.appspot.com\",\n    messagingSenderId: \"1257732943\",\n    appId: \"1:1257732943:web:e463fc0b6c0116f0\"\n};\nconst app = (0,firebase_app__WEBPACK_IMPORTED_MODULE_0__.initializeApp)(config);\nconst database = (0,firebase_firestore__WEBPACK_IMPORTED_MODULE_1__.getFirestore)(app);\nconst auth = (0,firebase_auth__WEBPACK_IMPORTED_MODULE_2__.getAuth)(app);\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9maXJlYmFzZS9jb25maWcudHN4LmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUE2QztBQUNLO0FBQ1Y7QUFFeEMsc0JBQXNCO0FBQ3RCLE1BQU1HLE1BQU0sR0FBRztJQUNiQyxNQUFNLEVBQUVDLHlDQUErQztJQUN2REcsVUFBVSxFQUFFSCxpQ0FBNEM7SUFDeERLLFNBQVMsRUFBRUwsaUJBQTJDO0lBQ3RETyxXQUFXLEVBQUUsd0NBQXdDO0lBQ3JEQyxhQUFhLEVBQUUsNkJBQTZCO0lBQzVDQyxpQkFBaUIsRUFBRSxZQUFZO0lBQy9CQyxLQUFLLEVBQUUsbUNBQW1DO0NBQzNDO0FBRU0sTUFBTUMsR0FBRyxHQUFHaEIsMkRBQWEsQ0FBQ0csTUFBTSxDQUFDLENBQUM7QUFDbEMsTUFBTWMsUUFBUSxHQUFHaEIsZ0VBQVksQ0FBQ2UsR0FBRyxDQUFDLENBQUM7QUFDbkMsTUFBTUUsSUFBSSxHQUFHaEIsc0RBQU8sQ0FBQ2MsR0FBRyxDQUFDLENBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9zZXJ2aWNlLW1hbmFnZXItd2ViLy4vZmlyZWJhc2UvY29uZmlnLnRzeD9jY2E0Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGluaXRpYWxpemVBcHAgfSBmcm9tICdmaXJlYmFzZS9hcHAnO1xuaW1wb3J0IHsgZ2V0RmlyZXN0b3JlIH0gZnJvbSAnZmlyZWJhc2UvZmlyZXN0b3JlJztcbmltcG9ydCB7IGdldEF1dGggfSBmcm9tICdmaXJlYmFzZS9hdXRoJztcblxuLy8gSW5pdGlhbGl6ZSBGaXJlYmFzZVxuY29uc3QgY29uZmlnID0ge1xuICBhcGlLZXk6IHByb2Nlc3MuZW52Lk5FWFRfUFVCTElDX0ZJUkVCQVNFX1BVQkxJQ19BUElfS0VZLFxuICBhdXRoRG9tYWluOiBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19GSVJFQkFTRV9BVVRIX0RPTUFJTixcbiAgcHJvamVjdElkOiBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19GSVJFQkFTRV9QUk9KRUNUX0lELFxuICBkYXRhYmFzZVVSTDogXCJodHRwczovL2JsdWVwb29scy1hNDlhOS5maXJlYmFzZWlvLmNvbVwiLFxuICBzdG9yYWdlQnVja2V0OiBcImJsdWVwb29scy1hNDlhOS5hcHBzcG90LmNvbVwiLFxuICBtZXNzYWdpbmdTZW5kZXJJZDogXCIxMjU3NzMyOTQzXCIsXG4gIGFwcElkOiBcIjE6MTI1NzczMjk0Mzp3ZWI6ZTQ2M2ZjMGI2YzAxMTZmMFwiXG59O1xuXG5leHBvcnQgY29uc3QgYXBwID0gaW5pdGlhbGl6ZUFwcChjb25maWcpO1xuZXhwb3J0IGNvbnN0IGRhdGFiYXNlID0gZ2V0RmlyZXN0b3JlKGFwcCk7XG5leHBvcnQgY29uc3QgYXV0aCA9IGdldEF1dGgoYXBwKTtcbiJdLCJuYW1lcyI6WyJpbml0aWFsaXplQXBwIiwiZ2V0RmlyZXN0b3JlIiwiZ2V0QXV0aCIsImNvbmZpZyIsImFwaUtleSIsInByb2Nlc3MiLCJlbnYiLCJORVhUX1BVQkxJQ19GSVJFQkFTRV9QVUJMSUNfQVBJX0tFWSIsImF1dGhEb21haW4iLCJORVhUX1BVQkxJQ19GSVJFQkFTRV9BVVRIX0RPTUFJTiIsInByb2plY3RJZCIsIk5FWFRfUFVCTElDX0ZJUkVCQVNFX1BST0pFQ1RfSUQiLCJkYXRhYmFzZVVSTCIsInN0b3JhZ2VCdWNrZXQiLCJtZXNzYWdpbmdTZW5kZXJJZCIsImFwcElkIiwiYXBwIiwiZGF0YWJhc2UiLCJhdXRoIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./firebase/config.tsx\n");

/***/ }),

/***/ "./pages/_app.tsx":
/*!************************!*\
  !*** ./pages/_app.tsx ***!
  \************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _styles_globals_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../styles/globals.css */ \"./styles/globals.css\");\n/* harmony import */ var _styles_globals_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_styles_globals_css__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _chakra_ui_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @chakra-ui/react */ \"@chakra-ui/react\");\n/* harmony import */ var _chakra_ui_react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_chakra_ui_react__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _context_authContext__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../context/authContext */ \"./context/authContext.tsx\");\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_context_authContext__WEBPACK_IMPORTED_MODULE_3__]);\n_context_authContext__WEBPACK_IMPORTED_MODULE_3__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];\n\n\n\n\n// 2. Extend the theme to include custom colors, fonts, etc\nconst colors = {\n    brand: {\n        vd: \"#2874a6\",\n        d: \"#2874a6\",\n        m: \"#3498db \",\n        l: \"#85c1e9\",\n        tomato: \"tomato\"\n    },\n    components: {\n        IconButton: {\n            colorScheme: {\n                tomato: {\n                    color: \"tomato\"\n                }\n            }\n        }\n    }\n};\nconst theme = (0,_chakra_ui_react__WEBPACK_IMPORTED_MODULE_2__.extendTheme)({\n    colors\n});\nfunction MyApp({ Component , pageProps  }) {\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_context_authContext__WEBPACK_IMPORTED_MODULE_3__.AuthProvider, {\n        children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_chakra_ui_react__WEBPACK_IMPORTED_MODULE_2__.ChakraProvider, {\n            theme: theme,\n            children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Component, {\n                ...pageProps\n            }, void 0, false, {\n                fileName: \"/Users/joshbell/Personal/bp-service-manager/service-manager-web/pages/_app.tsx\",\n                lineNumber: 31,\n                columnNumber: 9\n            }, this)\n        }, void 0, false, {\n            fileName: \"/Users/joshbell/Personal/bp-service-manager/service-manager-web/pages/_app.tsx\",\n            lineNumber: 30,\n            columnNumber: 7\n        }, this)\n    }, void 0, false, {\n        fileName: \"/Users/joshbell/Personal/bp-service-manager/service-manager-web/pages/_app.tsx\",\n        lineNumber: 29,\n        columnNumber: 5\n    }, this);\n}\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MyApp);\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9fYXBwLnRzeC5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUFBO0FBQStCO0FBQ2dDO0FBQ1Q7QUFFdEQsMkRBQTJEO0FBQzNELE1BQU1HLE1BQU0sR0FBRztJQUNiQyxLQUFLLEVBQUU7UUFDTEMsRUFBRSxFQUFFLFNBQVM7UUFDYkMsQ0FBQyxFQUFFLFNBQVM7UUFDWkMsQ0FBQyxFQUFFLFVBQVU7UUFDYkMsQ0FBQyxFQUFFLFNBQVM7UUFDWkMsTUFBTSxFQUFFLFFBQVE7S0FDakI7SUFDREMsVUFBVSxFQUFFO1FBQ1ZDLFVBQVUsRUFBRTtZQUNWQyxXQUFXLEVBQUU7Z0JBQ1hILE1BQU0sRUFBRTtvQkFDTkksS0FBSyxFQUFFLFFBQVE7aUJBQ2hCO2FBQ0Y7U0FDRjtLQUNGO0NBQ0Y7QUFFRCxNQUFNQyxLQUFLLEdBQUdiLDZEQUFXLENBQUM7SUFBRUUsTUFBTTtDQUFFLENBQUM7QUFFckMsU0FBU1ksS0FBSyxDQUFDLEVBQUVDLFNBQVMsR0FBRUMsU0FBUyxHQUFPLEVBQUU7SUFDNUMscUJBQ0UsOERBQUNmLDhEQUFZO2tCQUNYLDRFQUFDRiw0REFBYztZQUFDYyxLQUFLLEVBQUVBLEtBQUs7c0JBQzFCLDRFQUFDRSxTQUFTO2dCQUFFLEdBQUdDLFNBQVM7Ozs7O29CQUFJOzs7OztnQkFDYjs7Ozs7WUFDSixDQUNmO0NBQ0g7QUFFRCxpRUFBZUYsS0FBSyxFQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vc2VydmljZS1tYW5hZ2VyLXdlYi8uL3BhZ2VzL19hcHAudHN4PzJmYmUiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICcuLi9zdHlsZXMvZ2xvYmFscy5jc3MnO1xuaW1wb3J0IHsgQ2hha3JhUHJvdmlkZXIsIGV4dGVuZFRoZW1lIH0gZnJvbSAnQGNoYWtyYS11aS9yZWFjdCc7XG5pbXBvcnQgeyBBdXRoUHJvdmlkZXIgfSBmcm9tICcuLi9jb250ZXh0L2F1dGhDb250ZXh0JztcblxuLy8gMi4gRXh0ZW5kIHRoZSB0aGVtZSB0byBpbmNsdWRlIGN1c3RvbSBjb2xvcnMsIGZvbnRzLCBldGNcbmNvbnN0IGNvbG9ycyA9IHtcbiAgYnJhbmQ6IHtcbiAgICB2ZDogJyMyODc0YTYnLFxuICAgIGQ6ICcjMjg3NGE2JyxcbiAgICBtOiAnIzM0OThkYiAnLFxuICAgIGw6ICcjODVjMWU5JyxcbiAgICB0b21hdG86ICd0b21hdG8nLFxuICB9LFxuICBjb21wb25lbnRzOiB7XG4gICAgSWNvbkJ1dHRvbjoge1xuICAgICAgY29sb3JTY2hlbWU6IHtcbiAgICAgICAgdG9tYXRvOiB7XG4gICAgICAgICAgY29sb3I6ICd0b21hdG8nLFxuICAgICAgICB9LFxuICAgICAgfSxcbiAgICB9LFxuICB9LFxufTtcblxuY29uc3QgdGhlbWUgPSBleHRlbmRUaGVtZSh7IGNvbG9ycyB9KTtcblxuZnVuY3Rpb24gTXlBcHAoeyBDb21wb25lbnQsIHBhZ2VQcm9wcyB9OiBhbnkpIHtcbiAgcmV0dXJuIChcbiAgICA8QXV0aFByb3ZpZGVyPlxuICAgICAgPENoYWtyYVByb3ZpZGVyIHRoZW1lPXt0aGVtZX0+XG4gICAgICAgIDxDb21wb25lbnQgey4uLnBhZ2VQcm9wc30gLz5cbiAgICAgIDwvQ2hha3JhUHJvdmlkZXI+XG4gICAgPC9BdXRoUHJvdmlkZXI+XG4gICk7XG59XG5cbmV4cG9ydCBkZWZhdWx0IE15QXBwO1xuIl0sIm5hbWVzIjpbIkNoYWtyYVByb3ZpZGVyIiwiZXh0ZW5kVGhlbWUiLCJBdXRoUHJvdmlkZXIiLCJjb2xvcnMiLCJicmFuZCIsInZkIiwiZCIsIm0iLCJsIiwidG9tYXRvIiwiY29tcG9uZW50cyIsIkljb25CdXR0b24iLCJjb2xvclNjaGVtZSIsImNvbG9yIiwidGhlbWUiLCJNeUFwcCIsIkNvbXBvbmVudCIsInBhZ2VQcm9wcyJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./pages/_app.tsx\n");

/***/ }),

/***/ "./styles/globals.css":
/*!****************************!*\
  !*** ./styles/globals.css ***!
  \****************************/
/***/ (() => {



/***/ }),

/***/ "@chakra-ui/react":
/*!***********************************!*\
  !*** external "@chakra-ui/react" ***!
  \***********************************/
/***/ ((module) => {

"use strict";
module.exports = require("@chakra-ui/react");

/***/ }),

/***/ "nookies":
/*!**************************!*\
  !*** external "nookies" ***!
  \**************************/
/***/ ((module) => {

"use strict";
module.exports = require("nookies");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "firebase/app":
/*!*******************************!*\
  !*** external "firebase/app" ***!
  \*******************************/
/***/ ((module) => {

"use strict";
module.exports = import("firebase/app");;

/***/ }),

/***/ "firebase/auth":
/*!********************************!*\
  !*** external "firebase/auth" ***!
  \********************************/
/***/ ((module) => {

"use strict";
module.exports = import("firebase/auth");;

/***/ }),

/***/ "firebase/firestore":
/*!*************************************!*\
  !*** external "firebase/firestore" ***!
  \*************************************/
/***/ ((module) => {

"use strict";
module.exports = import("firebase/firestore");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/_app.tsx"));
module.exports = __webpack_exports__;

})();