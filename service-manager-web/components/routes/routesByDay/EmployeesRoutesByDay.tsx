import React from 'react';
import { useRouter } from 'next/router';
import { useEmployeeRoutesByDay } from '../Route-hooks';

export default function EmployeesRoutesByDay({ id, day, setDay, route }: any) {
  const router = useRouter();
  console.log('route', route);
  return (
    <>
      {route && (
        <>
          <button onClick={() => setDay('')} type="button">
            Back
          </button>

          <ol>
            {route.route.map((s: any, i: any) => {
                console.log('s', s);
                return (
                  <li key={i}>
                    {s.customerName}
                    <span>{s.customerAddress}</span>
                  </li>
                );
              })}
          </ol>
          <button
            type="button"
            onClick={() => router.push(`/routes/edit?id=${route.id}`)}
          >
            Edit
          </button>
        </>
      )}
    </>
  );
}