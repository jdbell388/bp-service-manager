import React, { useState } from 'react';
import { AddIcon, DeleteIcon, DragHandleIcon } from '@chakra-ui/icons';
import { Input, Button, Select, Heading, Box, Flex } from '@chakra-ui/react';
import { Container, Draggable } from 'react-smooth-dnd';
import {arrayMoveImmutable} from 'array-move';
import useForm from '../../form/hooks/Form-hooks';

// Todo: Import actual customers

export default function RouteForm({
  submitRouteForm,
  editRoute,
  removeRoute,
}: any) {

  const initialStops = editRoute ? editRoute.route : [];
  const initialDay = editRoute ? editRoute.day : '';
  
  const [values, handleUpdate, updateSingleVal] = useForm({
    day: initialDay,
    stops: initialStops
  });


  const addStop = (vals: any) => {
    updateSingleVal([...values.stops, vals], 'stops');
  };

  const removeStop = (index: number) => {
    const newStops = values.stops.splice(index, 1);
    updateSingleVal(newStops, 'stops');
  };

  const reorderStops = ({ removedIndex, addedIndex }: any) => {
    if (typeof values.stops !== 'undefined') {
      const newStops = arrayMoveImmutable(values.stops, removedIndex, addedIndex)
      updateSingleVal(newStops, 'stops');
    }
  };

  function getRandomInt(min: number, max: number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  const handleInputChange = (event: any) => {
    const { target } = event;
    const id = target.id.split('_');
    const row = id[0];
    const name = id[1];
    const val = target.value;
    let newVal = values.stops;
    newVal[row] = {
      ...values.stops[row],
      [name]: val
    }

    console.log("NEWVAL", newVal);
    
    updateSingleVal(newVal, 'stops');
  }

  return (
    <form>
      <Box mb="30px">
        <label htmlFor="day">Day</label>
        <Select
          name="day"
          id="day"
          onChange={handleUpdate}
          value={values.day}
          placeholder="-- Select Day --"
        >
          <option value="monday">Monday</option>
          <option value="tuesday">Tuesday</option>
          <option value="wednesday">Wednesday</option>
          <option value="thursday">Thursday</option>
          <option value="friday">Friday</option>
          <option value="saturday">Saturday</option>
        </Select>
      </Box>
      <Box mb="40px">
        <Heading as="h3" size="lg" mb="10px">
          Stops
        </Heading>
        <>
          <Container onDrop={reorderStops} render={() => (
          <>
              {
                values.stops.map((single: any, index: number) => (
                  <Draggable key={index} render={() => (
<Flex
                      justify="space-between"
                      alignContent="center"
                      alignItems="center"
                      mb="10px"
                    >
                      <Box>
                        <Heading as="h4" size="md">
                          {index + 1}
                          .
                        </Heading>
                      </Box>
                      <Box w="43%">
                        <Input
                          type="text"
                          value={single.customerName}
                          onChange={handleInputChange}
                          placeholder="Name"
                          name={`${index}_customerName`}
                          id={`${index}_customerName`}
                        />
                      </Box>
                      <Box w="43%">
                        <Input
                          type="text"
                          value={single.customerAddress}
                          onChange={handleInputChange}
                          placeholder="Address"
                          name={`${index}_customerAddress`}
                          id={`${index}_customerAddress`}
                        />
                      </Box>
                      <Box>
                        <Button
                          type="button"
                          bg="Tomato"
                          color="white"
                          onClick={() => removeStop(index)}
                        >
                          <DeleteIcon boxSize="3" mr="5px" />
                          {' '}
                          Remove Stop
                        </Button>
                      </Box>
                    </Flex>
                  )} />
                ))
              }
              </>
          )} />
          <Button
            type="button"
            bg="brand.m"
            color="white"
            mt="20px"
            onClick={() =>
              addStop({ customerName: '', customerAddress: '', id: getRandomInt(1, 10000) })
            }
          >
            <AddIcon boxSize="3" mr="5px" /> Add Stop
          </Button>
        </>
      </Box>
      <Button type="submit" bg="SeaGreen" color="white" mt="20px" onClick={(e) => {
        e.preventDefault();
        console.log('submitrouteform', values);
        submitRouteForm(values);
      }}>
        Submit Route
      </Button>
      {typeof removeRoute === 'function' && (
        <Button
          type="button"
          bg="Tomato"
          color="white"
          onClick={() => removeRoute()}
        >
          Remove Route
        </Button>
      )}
    </form>
  );
}
