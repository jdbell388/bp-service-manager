import React from 'react';
import { Input, Button } from '@chakra-ui/react';
import useForm from '../../form/hooks/Form-hooks';

function EmployeeForm(props: any) {
  const { submit, employee } = props;
  const [values, handleUpdate] = useForm({
    FirstName: employee ? employee.FirstName : '',
    LastName: employee ? employee.LastName : '',
    Address: employee ? employee.Address : '',
    Email: employee ? employee.Email : '',
    Phone: employee ? employee.Phone : '',
  });

  return (
    <form>
      <div>
        <label htmlFor="FirstName">First Name</label>
        <Input
          type="text"
          name="FirstName"
          id="FirstName"
          placeholder="FirstName"
          value={values.FirstName}
          onChange={handleUpdate}
          mb="10px"
        />
        <label htmlFor="LastName">Last Name</label>
        <Input
          name="LastName"
          id="LastName"
          placeholder="LastName"
          value={values.LastName}
          onChange={handleUpdate}
          mb="10px"
        />
        <label htmlFor="Address">Address</label>
        <Input
          type="text"
          name="Address"
          id="Address"
          placeholder="Address"
          value={values.Address}
          onChange={handleUpdate}
          mb="10px"
        />
        <label htmlFor="Email">Email</label>
        <Input
          type="text"
          name="Email"
          id="Email"
          placeholder="Email"
          value={values.Email}
          onChange={handleUpdate}
          mb="10px"
        />
        <label htmlFor="Phone">Phone</label>
        <Input
          type="text"
          name="Phone"
          id="Phone"
          placeholder="Phone"
          value={values.Phone}
          onChange={handleUpdate}
          mb="10px"
        />
        <div>
          <Button
            type="button"
            bg="SeaGreen"
            color="white"
            mt="20px"
            onClick={() =>
              submit(
                values.FirstName,
                values.LastName,
                values.Address,
                values.Email,
                values.Phone
              )
            }
          >
            Submit
          </Button>
        </div>
      </div>
    </form>
  );
}

export default EmployeeForm;
