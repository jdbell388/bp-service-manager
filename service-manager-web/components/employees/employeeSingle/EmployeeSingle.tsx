import React from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import { Button, Heading, Link } from '@chakra-ui/react';
import { DeleteIcon, SettingsIcon } from '@chakra-ui/icons';
import { useRouter } from 'next/router';
import { deleteEmployee } from '../../../firebase/employees';

export default function EmployeeSingle({ employee, id }: any) {
  const router = useRouter();

  const removeEmployee = async () => {
    await deleteEmployee(id);
    alert('Employee Deleted');
    router.push('/employees');
  };

  return (
    <AnimatePresence>
      {employee && (
        <motion.div
          initial={{ opacity: 0, y: 50 }}
          animate={{ opacity: 1, y: 0 }}
          exit={{ opacity: 0, y: 50 }}
          transition={{ duration: 0.5, delay: 0.3 }}
        >
          <div>
            <div>
              {employee.FirstName && (
                <p>
                  <strong>First Name: </strong>
                  {employee.FirstName}
                </p>
              )}
              {employee.LastName && (
                <p>
                  <strong>Last Name: </strong>
                  {employee.LastName}
                </p>
              )}
              {employee.Address && (
                <p>
                  <strong>Address: </strong> 
{' '}
{employee.Address}
                </p>
              )}
              {employee.Phone && (
                <p>
                  <strong>Phone: </strong> 
{' '}
{employee.Phone}
                </p>
              )}
              {employee.Email && (
                <p>
                  <strong>Email: </strong> 
{' '}
{employee.Email}
                </p>
              )}
              <Button
                p="10px"
                rounded="md"
                bg="SlateGray"
                color="AliceBlue"
                mt="20px"
                mr="20px"
                boxShadow="md"
                type="button"
                onClick={() => {
                  router.push(`/employees/edit?id=${id}`);
                }}
              >
                <SettingsIcon boxSize="3" /> Edit
              </Button>
              <Button
                p="10px"
                rounded="md"
                bg="Tomato"
                color="white"
                mt="20px"
                boxShadow="md"
                type="button"
                onClick={removeEmployee}
              >
                <DeleteIcon boxSize="3" />
{' '}
Delete
</Button>
            </div>
          </div>
        </motion.div>
      )}
    </AnimatePresence>
  );
}
