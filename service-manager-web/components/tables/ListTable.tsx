import React from 'react';
import { useTable } from 'react-table';

export default function ListTable({ columns, data }: any) {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({ columns, data });
  return (
    <table {...getTableProps()} style={{ border: 'solid 1px black', width: "100%" }}>
      <thead>
        {headerGroups.map((headerGroup: any, index: any) => (
          <tr {...headerGroup.getHeaderGroupProps()} key={index}>
            {headerGroup.headers.map((column: any, index: any) => (
              <th
                {...column.getHeaderProps()}
                key={index}
                style={{
                  color: 'black',
                  fontWeight: 'bold',
                  padding: '10px',
                }}
              >
                {column.render('Header')}
              </th>
            ))}
          </tr>
        ))}
      </thead>

      <tbody {...getTableBodyProps()}>
        {rows.map((row: any, index: any) => {
          prepareRow(row);

          return (
            <tr {...row.getRowProps()} key={index}>
              {row.cells.map((cell: any, index: any) => (
                <td
                  key={index}
                    {...cell.getCellProps()}
                    style={{
                      padding: '10px',
                      border: '1px solid black',
                    }}
                  >
                    {cell.render('Cell')}
                  </td>
              ))}
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
