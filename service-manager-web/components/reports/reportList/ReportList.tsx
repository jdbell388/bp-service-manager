import React, { useMemo } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import Link from 'next/link';
import { ListTable } from '../../tables';
import { ReportInt } from '../../../firebase/reports';

export default function ReportList({ reports }: any) {
  const data = useMemo(
    () =>
      reports.map((report: ReportInt) => ({
        address: report.address,
        customer: report.customer,
        chlorine: report.chlorine,
        chlAdded: report.chlAdded,
        ph: report.ph,
        phAdded: report.phAdded,
        ta: report.ta,
        taAdded: report.taAdded,
        link: `/reports/report?id=${report.id}`,
      })),
    []
  );
  const columns = useMemo(
    () => [
      {
        Header: 'Customer',
        accessor: 'customer',
      },
      {
        Header: 'Address',
        accessor: 'address',
      },
      {
        Header: "Chl",
        accessor: 'chlorine',
      },
      {
        Header: "Chl Add",
        accessor: 'chlAdded',
      },
      {
        Header: "PH",
        accessor: 'ph',
      },
      {
        Header: "PH Add",
        accessor: 'phAdded',
      },
      {
        Header: "TA",
        accessor: 'ta',
      },
      {
        Header: "Ta Add",
        accessor: 'taAdded',
      },
      {
        Header: 'Link',
        accessor: 'link',
        Cell: (row: any) => <Link href={row.value}>Details</Link>,
      },
    ],
    []
  );
  return (
    <AnimatePresence>
      {reports ? (
        // <motion.div
        //   initial={{ opacity: 0, y: 50 }}
        //   animate={{ opacity: 1, y: 0 }}
        //   exit={{ opacity: 0, y: 50 }}
        //   transition={{ duration: 0.5, delay: 0.3 }}
        // >
          <ListTable columns={columns} data={data} />
        // </motion.div>
      ) : (
        <h1>No Reports</h1>
      )}
    </AnimatePresence>
  );
}
