import React from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import { Button, Heading, Link } from '@chakra-ui/react';
import { DeleteIcon, SettingsIcon } from '@chakra-ui/icons';
import { useRouter } from 'next/router';
import { deleteReport } from '../../../firebase/reports';

export default function ReportSingle({ report, id, title }: any) {
  const router = useRouter();

  const removeReport = async () => {
    await deleteReport(id);
    alert('report Deleted');
    router.push('/reports');
  };

  return (
    <AnimatePresence>
      {report ? (
        <motion.div
          initial={{ opacity: 0, y: 50 }}
          animate={{ opacity: 1, y: 0 }}
          exit={{ opacity: 0, y: 50 }}
          transition={{ duration: 0.5, delay: 0.3 }}
        >
          <div>
            {report.FirstName && (
              <p>
                <strong>First Name: </strong>
                {report.FirstName}
              </p>
            )}
            {report.LastName && (
              <p>
                <strong>Last Name: </strong>
                {report.LastName}
              </p>
            )}
            {report.Address && (
              <p>
                <strong>Address: </strong> 
{' '}
{report.Address}
              </p>
            )}
            {report.Employee && (
              <p>
                <strong>Employee: </strong> {report.Employee?.name}
              </p>
            )}
            {report.Phone && (
              <p>
                <strong>Phone: </strong> 
{' '}
{report.Phone}
              </p>
            )}
            {report.Email && (
              <p>
                <strong>Email: </strong> {report.Email}
              </p>
            )}
            <Button
              p="10px"
              rounded="md"
              bg="SlateGray"
              color="AliceBlue"
              mt="20px"
              mr="20px"
              boxShadow="md"
              type="button"
              onClick={() => {
                router.push(`/reports/edit?id=${id}`);
              }}
            >
              <SettingsIcon boxSize="3" /> Edit
            </Button>
            <Button
              p="10px"
              rounded="md"
              bg="Tomato"
              color="white"
              mt="20px"
              boxShadow="md"
              type="button"
              onClick={removeReport}
            >
              <DeleteIcon boxSize="3" /> Delete
            </Button>
          </div>
        </motion.div>
      ) : (
        <p>Loading...</p>
      )}
    </AnimatePresence>
  );
}
