import React, { useState } from 'react';

export default function useForm(initialValues: any) {
  const [values, setValues] = useState(initialValues);

  const handleUpdate = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { target } = e;
    setValues({
      ...values,
      [target.id]: target.value,
    });
  };

  const updateSingleVal = (val: any, id: string) => {
    setValues({
      ...values,
      [id]: val,
    });
  };

  return [values, handleUpdate, updateSingleVal];
}
