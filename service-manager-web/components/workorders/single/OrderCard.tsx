import React from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import { Button, Heading, Link } from '@chakra-ui/react';
import { DeleteIcon, SettingsIcon, CheckIcon } from '@chakra-ui/icons';
import { useRouter } from 'next/router';
import { deleteOrder, markComplete } from '../../../firebase/workorders';

export default function OrderCard({ order }: any) {
  const { id } = order;
  const router = useRouter();
  const removeOrder = async () => {
    await deleteOrder(id);
    alert('order Deleted');
    router.push('/workorders');
  };
  const handleComplete = async () => {
    await markComplete(id);
    alert('Order Completed');
    router.push(`/workorders/order?id=${id}`);
  };
  return (
    <>
      {order && (
        <div>
          <div>
            {order.Employee && (
              <p>
                <strong>Employee: </strong> 
{' '}
{order.Employee.name}
              </p>
            )}
            {order.Status && (
              <p>
                <strong>Status: </strong> <span>{order.Status}</span>
              </p>
            )}
            {order.account && (
              <p>
                <strong>Account: </strong> 
{' '}
{order.account.name}
              </p>
            )}
            {order.Description && (
              <p>
                <strong>Description: </strong> {order.Description}
              </p>
            )}
            {order.tags && (
              <p>
                <strong>Tags: </strong>
                {order.tags.map((tag: string) => `${tag}, `)}
              </p>
            )}
            <Button
              p="10px"
              rounded="md"
              bg="SeaGreen"
              color="white"
              mt="20px"
              mr="20px"
              boxShadow="md"
              type="button"
              onClick={handleComplete}
            >
              <CheckIcon boxSize="3" /> Complete
            </Button>
            <Button
              p="10px"
              rounded="md"
              bg="SlateGray"
              color="AliceBlue"
              mt="20px"
              mr="20px"
              boxShadow="md"
              type="button"
              onClick={() => {
                router.push(`/workorders/edit?id=${id}`);
              }}
            >
              <SettingsIcon boxSize="3" /> Edit
            </Button>
            <Button
              p="10px"
              rounded="md"
              bg="Tomato"
              color="white"
              mt="20px"
              boxShadow="md"
              type="button"
              onClick={removeOrder}
            >
              <DeleteIcon boxSize="3" />
{' '}
Delete
</Button>
          </div>
        </div>
      )}
    </>
  );
}
