import React, { useMemo } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import Link from 'next/link';
import { OrderInt } from '../../../firebase/workorders';
import { ListTable } from '../../tables';

export default function OrderList({ orders }: any) {
  const data = useMemo(
    () =>
      orders.map((order: OrderInt) => ({
        title: order.Title,
        status: order.Status,
        description: order.Description,
        employee: order.Employee?.name,
        account: order.account?.name,
        link: `/workorders/order?id=${order.id}`,
      })),
    []
  );
  const columns = useMemo(
    () => [
      {
        Header: 'Title',
        accessor: 'title',
      },
      {
        Header: 'Status',
        accessor: 'status',
      },
      {
        Header: 'Description',
        accessor: 'description',
      },
      {
        Header: 'Employee',
        accessor: 'employee',
      },
      {
        Header: 'Account',
        accessor: 'account',
      },
      {
        Header: 'Link',
        accessor: 'link',
        Cell: (row: any) => <Link href={row.value}>Details</Link>,
      },
    ],
    []
  );
  return (
    <AnimatePresence>
      {orders.length > 0 ? (
        // <motion.div
        //   initial={{ opacity: 0, y: 50 }}
        //   animate={{ opacity: 1, y: 0 }}
        //   exit={{ opacity: 0, y: 50 }}
        //   transition={{ duration: 0.5, delay: 0.3 }}
        // >
          <ListTable columns={columns} data={data} />
        // </motion.div>
      ) : (
        <p>no orders</p>
      )}
    </AnimatePresence>
  );
}
