import React from 'react';
import { Box, VStack, Link, Icon, Heading, Flex, Image } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { AiOutlineHome, AiOutlineTeam } from 'react-icons/ai';
import { BiTask } from 'react-icons/bi';
import { FaRoute } from 'react-icons/fa';
import { MdPool } from 'react-icons/md';

const SidebarItem = ({ title, link, icon }: any) => {
  const router = useRouter();
  const isActive = router.pathname === link;
  return (
    <Link
      href={link}
      w="100%"
      p="20px"
      _hover={{ background: 'white', boxShadow: 'lg', color: 'black' }}
      bg={isActive ? 'brand.m' : ''}
      color={isActive ? 'white' : 'black'}
      rounded="md"
      boxShadow={isActive ? 'lg' : ''}
      alignItems="center"
      display="flex"
    >
      <span style={{ marginRight: '10px' }}>{icon}</span>
    {title}
    </Link>
  );
};

export default function Sidebar() {
  const size = '7';
  return (
    <Box bg="gray.50" p="20px">
      <Flex
        boxShadow="sm"
        p="20px"
        rounded="md"
        bg="white"
        mb="30px"
        alignItems="center"
      >
        <Image src="/img/blue-pools-icon.png" boxSize="40px" />
        <Heading as="h1" size="md" pl="10px">
          Service Manager
        </Heading>
      </Flex>
      <VStack>
        <SidebarItem
          link="/"
          title="Dashboard"
          icon={<Icon as={AiOutlineHome} boxSize={size} />}
        />
        <SidebarItem
          link="/customers"
          title="Customers"
          icon={<Icon as={MdPool} boxSize={size} />}
        />
        <SidebarItem
          link="/employees"
          title="Employees"
          icon={<Icon as={AiOutlineTeam} boxSize={size} />}
        />
        <SidebarItem
          link="/routes"
          title="Routes"
          icon={<Icon as={FaRoute} boxSize={size} />}
        />
        <SidebarItem
          link="/workorders"
          title="Workorders"
          icon={<Icon as={BiTask} boxSize={size} />}
        />
      </VStack>
    </Box>
  );
}
