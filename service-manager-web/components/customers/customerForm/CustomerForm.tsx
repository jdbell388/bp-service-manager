import React, { useState, useEffect } from 'react';
import { Input, Button } from '@chakra-ui/react';
import * as R from 'rambda';

import { CustomerInt } from '../../../firebase/customers';
import { EmployeeInt } from '../../../firebase/employees';
import useForm from '../../form/hooks/Form-hooks';
import RelatedField from '../../form/RelatedField';
import { getAllObj } from '../../../firebase/utils';

interface Props {
  submit: Function;
  customer?: CustomerInt;
}

function CustomerForm(props: Props) {
  const [Employees, setEmployees] = useState<any>();
  const { submit, customer } = props;
  const [values, handleUpdate, updateSingleVal] = useForm({
    FirstName: customer ? customer.FirstName : '',
    LastName: customer ? customer.LastName : '',
    Address: customer ? customer.Address : '',
    Employee: customer ? customer.Employee : '',
    Email: customer ? customer.Email : '',
    Phone: customer ? customer.Phone : '',
  });

  const getEmployees = async () => {
    const results = await getAllObj('employees');
    const predicate = (single: any) => {
      const obj = {
        id: single.id,
        name: `${single.data.FirstName} ${single.data.LastName}`,
      };
      return obj;
    };
    const returnArr = await R.map(predicate, results);
    setEmployees(returnArr);
  };

  useEffect(() => {
    getEmployees();
  }, []);

  return (
    <form>
      <label htmlFor="FirstName">First Name</label>
      <Input
        type="text"
        name="FirstName"
        id="FirstName"
        placeholder="FirstName"
        value={values.FirstName}
        onChange={handleUpdate}
        mb="10px"
      />
      <label htmlFor="LastName">Last Name</label>
      <Input
        name="LastName"
        id="LastName"
        placeholder="LastName"
        value={values.LastName}
        onChange={handleUpdate}
        mb="10px"
      />
      <label htmlFor="Address">Address</label>
      <Input
        type="text"
        name="Address"
        id="Address"
        placeholder="Address"
        value={values.Address}
        onChange={handleUpdate}
        mb="10px"
      />
      <label htmlFor="Email">Email</label>
      <Input
        type="text"
        name="Email"
        id="Email"
        placeholder="Email"
        value={values.Email}
        onChange={handleUpdate}
        mb="10px"
      />
      <label htmlFor="Phone">Phone</label>
      <Input
        type="text"
        name="Phone"
        id="Phone"
        placeholder="Phone"
        value={values.Phone}
        onChange={handleUpdate}
        mb="10px"
      />
      {Employees && (
        <RelatedField
          posts={Employees}
          label="Employee"
          initialValue={values.Employee}
          onChange={(val: EmployeeInt) => {
            updateSingleVal(val, 'Employee');
          }}
        />
      )}
      <Button
        type="button"
        bg="SeaGreen"
        color="white"
        mt="20px"
        onClick={() =>
          submit(
            values.FirstName,
            values.LastName,
            values.Address,
            values.Email,
            values.Employee,
            values.Phone
          )
        }
      >
        Submit
      </Button>
    </form>
  );
}

export default CustomerForm;
