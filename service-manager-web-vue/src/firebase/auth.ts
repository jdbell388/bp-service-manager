import { signInWithEmailAndPassword, createUserWithEmailAndPassword, signOut } from 'firebase/auth'
import { auth } from './config'

export const login = (email: any, password: any) => {
  signInWithEmailAndPassword(auth, email, password)
    .then(async userCredential => {
      const user = userCredential.user
      if (user) {
        const userToken = await user.getIdToken()
        console.log('idtoken', userToken)
      }
    })
    .catch(error => {
      const errorCode = error.code
      const errorMessage = error.message
      console.log('errorCode', errorCode, 'errorMessage', errorMessage)
    })
}

export const logout = () => {
  signOut(auth)
    .then(() => {})
    .catch(error => {
      console.log('error signing out: ', error)
    })
}

export const createUser = (email: string, password: string) => {
  createUserWithEmailAndPassword(auth, email, password)
    .then(userCredential => {
      const user = userCredential.user
    })
    .catch(error => {
      const errorCode = error.code
      const errorMessage = error.message
      console.log('errorCode', errorCode, 'errorMessage', errorMessage)
    })
}

export const checkAuthentication = (ctx: any) => {
  // const { __session, token } = cookies;
  // console.log('checkingauth', __session);
  // If there's no token, it means the user is not logged in.
  // if (!cookies) {
  //   if (typeof window === 'undefined') {
  //     ctx.res.writeHead(302, { Location: '/login' });
  //     ctx.res.end();
  //   } else {
  //     Router.push('/login');
  //   }
  // }
}

// export const withAuthSync = (WrappedComponent: any) => {
//   const Wrapper = (props: any) => {
//     const syncLogout = (event: any) => {
//       if (event.key === 'logout') {
//         console.log('logged out from storage!');
//       }
//     };

//     useEffect(() => {
//       window.addEventListener('storage', syncLogout);

//       return () => {
//         window.removeEventListener('storage', syncLogout);
//         window.localStorage.removeItem('logout');
//       };
//     }, []);

//     return <WrappedComponent {...props} />;
//   };

//   return Wrapper;
// };

export const protectPage = (context: any) => {
  const token = checkAuthentication(context)
  return token
}
