import { collection, doc, setDoc, getDocs, getDoc, deleteDoc } from 'firebase/firestore'
import { db } from '../firebase/config'

export const getAllObj = async (objectType: string) => {
  try {
    const objectList = await getDocs(collection(db, 'accounts'))
    const results: {}[] = []
    objectList.forEach(doc => {
      results.push({ id: doc.id, data: doc.data() })
    })
    return results
  } catch (e) {
    console.error(`Error getting ${objectType}: `, e)
  }
  return []
}
