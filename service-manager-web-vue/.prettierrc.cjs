module.exports = {
  printWidth: 120,
  semi: false,
  arrowParens: 'avoid',
  singleQuote: true,
  trailingComma: 'all',
  // Prettier Import Configs
  // prettier-ignore
  importOrder: [
    // Service files
    '^.*service.*$',
    // All other files starting with ./ or @
    '^([./]|@/(.*)$)',
  ],
  importOrderSeparation: true,
};