import React from 'react';
import { Link } from '@chakra-ui/react';
import { AddIcon } from '@chakra-ui/icons';
import { CustomerList } from '../components/customers/index';
import { getAllCustomers } from '../firebase/customers';
import { Dashboard } from '../components/view';

export default function customers({ results }: any) {
  return (
    <Dashboard title="Customers">
      {results && <CustomerList customers={results} />}
      <Link
        href="/customers/new"
        p="10px"
        rounded="md"
        bg="LightGreen"
        d="inline-block"
        mt="20px"
        boxShadow="md"
      >
        <AddIcon boxSize="3" /> New Customer
      </Link>
    </Dashboard>
  );
}

export async function getServerSideProps() {
  try {
    const results = await getAllCustomers();
    return { props: { results } };
  } catch (e) {
    return false;
  }
}
