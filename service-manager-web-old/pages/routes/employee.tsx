import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { Button, Heading, Link } from '@chakra-ui/react';
import { AddIcon, ArrowBackIcon } from '@chakra-ui/icons';
import { useEmployeeRoutes } from '../../components/routes/Route-hooks';
import { RouteInt } from '../../firebase/routes';
import { getSingleEmployee } from '../../firebase/employees';
import { EmployeesRoutesByDay } from '../../components/routes';
import { Dashboard } from '../../components/view';

interface Day {
  day: any;
}

export default function EmployeesRoutes({ employee, id }: any) {
  const router = useRouter();
  const [routes] = useEmployeeRoutes(id);
  const [sortedRoutes, setSortedRoutes] = useState(routes);
  const [day, setDay] = useState<any>();

  const sortByDay = (a: Day, b: Day) => {
    const day1 = a.day.toLowerCase();
    const day2 = b.day.toLowerCase();
    const sorter = {
      sunday: 0,
      monday: 1,
      tuesday: 2,
      wednesday: 3,
      thursday: 4,
      friday: 5,
      saturday: 6,
    };
    const result = sorter[day1] - sorter[day2];
    return result;
  };
  useEffect(() => {
    const newRoutes = routes.data?.sort(sortByDay);
    setSortedRoutes(newRoutes);
  }, [routes]);

  const title = day
    ? `${employee.FirstName} ${employee.LastName}'s ${day} Route`
    : `${employee.FirstName} ${employee.LastName}'s Routes`;
  return (
    <Dashboard
      title={title}
      backButton={(
        <Link href="/routes/">
          <ArrowBackIcon /> Back
        </Link>
      )}
    >
      {routes.isLoading && <h1>Loading</h1>}
      {day ? (
        <EmployeesRoutesByDay day={day} id={id} setDay={setDay} />
      ) : (
        <>
          {routes.data && (
            <>
              {routes.data.length > 0 && (
                <div>
                  <Heading as="h3" size="md" mb="20px">
                    Select A Day
                  </Heading>
                  <ul>
                    {sortedRoutes?.length &&
                      sortedRoutes.map((single: RouteInt, index: number) => (
                        <Button
                          key={single.day}
                          type="button"
                          bg="brand.d"
                          color="white"
                          mr="10px"
                          _hover={{ background: '#333' }}
                          onClick={() => setDay(single.day)}
                        >
                          {single.day}
                        </Button>
                      ))}
                  </ul>
                </div>
              )}

              <div>
                <Button
                  p="10px"
                  rounded="md"
                  bg="LightGreen"
                  d="inline-block"
                  mt="20px"
                  boxShadow="md"
                  type="button"
                  onClick={() => {
                    router.push(`/routes/new?id=${id}`);
                  }}
                >
                  <AddIcon boxSize="3" /> Add Route
                </Button>
              </div>
            </>
          )}
        </>
      )}
    </Dashboard>
  );
}

export async function getServerSideProps(context: any) {
  const { id } = context.query;
  try {
    const employee = await getSingleEmployee(id);
    return { props: { employee, id } };
  } catch (e) {
    return false;
  }
}
