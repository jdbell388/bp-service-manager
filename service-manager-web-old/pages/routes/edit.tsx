import React from 'react';
import { useRouter } from 'next/router';
import { ArrowBackIcon } from '@chakra-ui/icons';
import { Link } from '@chakra-ui/react';
import { RouteForm } from '../../components/routes';
import { updateRoute, getSingleRoute } from '../../firebase/routes';
import { Dashboard } from '../../components/view';

export default function EditRoute({ route }: any) {
  const router = useRouter();
  const { id } = router.query;
  const submitForm = async (
    FirstName: any,
    LastName: any,
    Address: any,
    Email: any,
    Phone: any
  ) => {
    await updateRoute({ FirstName, LastName, Address, Email, Phone, id });
    alert('Updated');
    router.push(`/routes/route?id=${id}`);
  };
  return (
    <Dashboard
      title="Edit Route"
      backButton={
        <Link href={`/routes/route?id=${id}`}>
          <ArrowBackIcon />
{' '}
Back
</Link>
      }
    >
      {route && <RouteForm submitRouteForm={submitForm} editRoute={route} />}
    </Dashboard>
  );
}

export async function getServerSideProps(context: any) {
  const { id } = context.query;
  try {
    const route = await getSingleRoute(id);
    return { props: { route } };
  } catch (e) {
    return false;
  }
}
