import React from 'react';
import { useRouter } from 'next/router';
import { ArrowBackIcon } from '@chakra-ui/icons';
import { Link } from '@chakra-ui/react';
import { RouteForm } from '../../components/routes';
import { addRoute } from '../../firebase/routes';
import { getSingleEmployee } from '../../firebase/employees';
import { Dashboard } from '../../components/view';
import { Anyfy } from 'rambda/_ts-toolbelt/src/Object/_Internal';

export default function newRoute({ employee, id }: any) {
  const router = useRouter();
  const employeeName = `${employee.FirstName} ${employee.LastName}`;
  const submitForm = async (values: any) => {
    const routeObj = {
      day: values.day,
      employeeId: id,
      employeeName,
      route: values.customers,
    };
    await addRoute(routeObj);
    alert('Route Added');
    router.push(`/routes/employee?id=${id}`);
  };
  return (
    <Dashboard
      title={`New Route for
      ${employeeName}`}
      backButton={(
        <Link href="/routes/">
          <ArrowBackIcon />
          {' '}
Back
        </Link>
      )}
    >
      <RouteForm submitRouteForm={submitForm} />
    </Dashboard>
  );
}

export async function getServerSideProps(context: any) {
  const { id } = context.query;
  try {
    const employee = await getSingleEmployee(id);
    return { props: { employee, id } };
  } catch (e) {
    return false;
  }
}
