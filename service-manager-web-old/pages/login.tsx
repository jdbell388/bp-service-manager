import React, { useState } from 'react';
import {
  InputGroup,
  Input,
  InputRightElement,
  Button,
  Flex,
  Box,
  Image,
  Heading,
} from '@chakra-ui/react';
import { motion, AnimatePresence } from 'framer-motion';
import { signin } from '../config/auth';
import firebase from '../firebase/config';

export default function login() {
  const [userData, setUserData] = useState({
    email: '',
    password: '',
    error: '',
  });
  const [show, setShow] = useState(false);

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    const { email, password } = userData;
    try {
      const signedIn = await signin(email, password);
      return signedIn;
    } catch (error) {
      console.log(error);
      if (error.code === 'auth/invalid-email') {
        alert('You have entered an invalid Email address');
      }
      if (error.code === 'auth/wrong-password') {
        alert('Incorrect email or password. Please try again.');
      }
    }
  };

  const handleChange = (change: any) => {
    const { id } = change.target;
    const { value } = change.target;
    setUserData({ ...userData, [id]: value });
  };

  const showHidePW = () => setShow(!show);

  const resetPassword = () => {
    const { email } = userData;
    firebase
      .auth()
      .sendPasswordResetEmail(email)
      .then(() => {
        // Password reset email sent!
        // ..
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        // ..
      });
  };

  return (
    <Flex
      w="100%"
      h="100vh"
      alignItems="center"
      justifyContent="center"
      flexWrap="wrap"
      alignContent="center"
    >
      <AnimatePresence>
        <motion.div
          initial={{ opacity: 0, y: 30 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.3, delay: 0 }}
        >
          <Flex w="100%" alignItems="center" justifyContent="center" mb="30px">
            <Image
              src="/img/blue-pools-icon.png"
              width="80px"
              height="88px"
              mr="10px"
            />
            <Heading as="h1" size="lg" pl="10px">
              Service Manager
            </Heading>
          </Flex>
          <Box
            maxW="md"
            overflow="hidden"
            p="30px"
            boxShadow="md"
            borderWidth="1px"
          >
            <form onSubmit={(e) => handleSubmit(e)} className="login-form">
              <InputGroup size="md">
                <Input
                  type="email"
                  id="email"
                  onChange={handleChange}
                  placeholder="Email"
                  value={userData.email}
                  mb="15px"
                />
              </InputGroup>
              <InputGroup size="md">
                <Input
                  pr="4.5rem"
                  type={show ? 'text' : 'password'}
                  placeholder="Password"
                  id="password"
                  value={userData.password}
                  onChange={handleChange}
                  mb="15px"
                />
                <InputRightElement width="4.5rem">
                  <Button h="1.75rem" size="sm" onClick={showHidePW}>
                    {show ? 'Hide' : 'Show'}
                  </Button>
                </InputRightElement>
              </InputGroup>
              <Button
                className="login"
                type="submit"
                w="100%"
                bg="brand.d"
                color="white"
              >
                Login
              </Button>
            </form>
            <p>Forgot Password?</p>
            <Button onClick={resetPassword}>Reset Password</Button>
          </Box>
        </motion.div>
      </AnimatePresence>
    </Flex>
  );
}
