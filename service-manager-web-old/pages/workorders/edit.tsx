import React from 'react';
import { useRouter } from 'next/router';
import { ArrowBackIcon } from '@chakra-ui/icons';
import { Link } from '@chakra-ui/react';
import { OrderForm } from '../../components/workorders';
import { updateOrder, getSingleOrder } from '../../firebase/workorders';
import { Dashboard } from '../../components/view';

export default function EditWorkorder({ order }: any) {
  const router = useRouter();
  const { id } = router.query;
  const submitForm = async (
    Title: string,
    Description: string,
    Employee: any,
    account: any,
    tags: any
  ) => {
    await updateOrder({ Title, Description, Employee, account, tags, id });
    alert('Updated');
    router.push(`/workorders?id=${id}`);
  };
  return (
    <Dashboard
      title="Edit Workorder"
      backButton={
        <Link href="/workorders/">
          <ArrowBackIcon />
{' '}
Back
</Link>
      }
    >
      {order && <OrderForm submit={submitForm} order={order} />}
    </Dashboard>
  );
}

export async function getServerSideProps(context: any) {
  const { id } = context.query;
  try {
    const order = await getSingleOrder(id);
    return { props: { order } };
  } catch (e) {
    return false;
  }
}
