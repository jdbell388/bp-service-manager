import React from 'react';
import { useRouter } from 'next/router';
import { ArrowBackIcon } from '@chakra-ui/icons';
import { Link } from '@chakra-ui/react';
import { OrderForm } from '../../components/workorders';
import { addOrder } from '../../firebase/workorders';
import { Dashboard } from '../../components/view';

export default function newWorkorder() {
  const router = useRouter();
  const submitForm = async (
    Title: any,
    Description: any,
    Employee: any,
    account: any,
    tags: any
  ) => {
    console.log({ Title, Description, Employee, account, tags });
    await addOrder({ Title, Description, Employee, account, tags });
    alert('added');
    router.push('/workorders');
  };
  return (
    <Dashboard
      title="New Workorder"
      backButton={(
        <Link href="/workorders/">
          <ArrowBackIcon /> Back
        </Link>
      )}
    >
      <OrderForm submit={submitForm} />
    </Dashboard>
  );
}
