import React from 'react';
import { Link } from '@chakra-ui/react';
import { AddIcon } from '@chakra-ui/icons';
import { EmployeeList } from '../components/employees/index';
import { getAllEmployees } from '../firebase/employees';
import { Dashboard } from '../components/view';

export default function employees({ results }: any) {
  return (
    <Dashboard title="Employees">
      {results && <EmployeeList employees={results} />}
      <Link
        href="/employees/new"
        p="10px"
        rounded="md"
        bg="LightGreen"
        d="inline-block"
        mt="20px"
        boxShadow="md"
      >
        <AddIcon boxSize="3" /> New Employee
      </Link>
    </Dashboard>
  );
}

export async function getServerSideProps() {
  try {
    const results = await getAllEmployees();
    return { props: { results } };
  } catch (e) {
    return false;
  }
}
