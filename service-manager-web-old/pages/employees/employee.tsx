import React from 'react';
import { Link } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { ArrowBackIcon } from '@chakra-ui/icons';
import { EmployeeSingle } from '../../components/employees/index';
import { getSingleEmployee } from '../../firebase/employees';
import { Dashboard } from '../../components/view';

export default function employee({ results }: any) {
  const router = useRouter();
  const { id } = router.query;
  return (
    <Dashboard
      title={`${results.FirstName} 
    ${results.LastName}`}
      backButton={(
        <Link href="/employees/">
          <ArrowBackIcon />
{' '}
Back
</Link>
      )}
    >
      <EmployeeSingle employee={results} id={id} />
    </Dashboard>
  );
}

export async function getServerSideProps(context: any) {
  const { id } = context.query;
  try {
    const results = await getSingleEmployee(id);
    return { props: { results } };
  } catch (e) {
    return false;
  }
}
