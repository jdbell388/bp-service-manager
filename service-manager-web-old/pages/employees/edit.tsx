import React from 'react';
import { useRouter } from 'next/router';
import { ArrowBackIcon } from '@chakra-ui/icons';
import { Link } from '@chakra-ui/react';
import { EmployeeForm } from '../../components/employees/employeeForm';
import { updateEmployee, getSingleEmployee } from '../../firebase/employees';
import { Dashboard } from '../../components/view';

export default function EditEmployee({ employee }: any) {
  const router = useRouter();
  const { id } = router.query;
  const submitForm = async (
    FirstName: any,
    LastName: any,
    Address: any,
    Email: any,
    Phone: any
  ) => {
    await updateEmployee({ FirstName, LastName, Address, Email, Phone, id });
    alert('Updated');
    router.push(`/employees/employee?id=${id}`);
  };
  console.log('employee', employee);
  return (
    <Dashboard
      title="Edit Employee"
      backButton={(
        <Link href="/employees/">
          <ArrowBackIcon />
{' '}
Back
</Link>
      )}
    >
      {employee && <EmployeeForm submit={submitForm} employee={employee} />}
    </Dashboard>
  );
}

export async function getServerSideProps(context: any) {
  const { id } = context.query;
  try {
    const employee = await getSingleEmployee(id);
    return { props: { employee } };
  } catch (e) {
    return false;
  }
}
