import React from 'react';
import { useRouter } from 'next/router';
import { ArrowBackIcon } from '@chakra-ui/icons';
import { Link } from '@chakra-ui/react';
import { EmployeeForm } from '../../components/employees/employeeForm';
import { addEmployee } from '../../firebase/employees';
import { Dashboard } from '../../components/view';

export default function newEmployee() {
  const router = useRouter();
  const submitForm = async (
    FirstName: any,
    LastName: any,
    Address: any,
    Email: any,
    Phone: any
  ) => {
    await addEmployee({
      FirstName,
      LastName,
      Address,
      Email,
      Phone,
    });
    alert('added');
    router.push('/employees');
  };
  return (
    <Dashboard
      title="New Employee"
      backButton={
        <Link href="/employees/">
          <ArrowBackIcon /> Back
        </Link>
      }
    >
      <EmployeeForm submit={submitForm} />
    </Dashboard>
  );
}
