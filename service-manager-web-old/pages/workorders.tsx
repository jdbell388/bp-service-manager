import React from 'react';
import { Link } from '@chakra-ui/react';
import { AddIcon } from '@chakra-ui/icons';
import { getAllOrders } from '../firebase/workorders';
import { OrderList } from '../components/workorders';
import { Dashboard } from '../components/view';

export default function workorders({ orders }: any) {
  return (
    <Dashboard title="Work Orders">
      {orders.length > 0 && <OrderList orders={orders} />}
      <Link
        href="/workorders/new"
        p="10px"
        rounded="md"
        bg="LightGreen"
        d="inline-block"
        mt="20px"
        boxShadow="md"
      >
        <AddIcon boxSize="3" />
{' '}
New Work Order
</Link>
    </Dashboard>
  );
}

export async function getServerSideProps() {
  try {
    const orders = await getAllOrders();
    return { props: { orders } };
  } catch (e) {
    return false;
  }
}
