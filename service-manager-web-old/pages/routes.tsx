import React, { useState } from 'react';
import Link from 'next/link';
import { Router, useRouter } from 'next/router';
import { Heading, Button } from '@chakra-ui/react';
import { getAllEmployees, EmployeeInt } from '../firebase/employees';
import { Dashboard } from '../components/view';

export default function employeeRoutes({ employees }: any) {
  const router = useRouter();
  return (
    <Dashboard title="Routes">
      <Heading as="h3" size="md" mb="20px">
        Select An Employee
      </Heading>
      <ul>
        {employees &&
          employees.map((single: EmployeeInt, index: number) => (
            <Button
              type="button"
              bg="brand.d"
              color="white"
              mr="10px"
              _hover={{ background: '#333' }}
              onClick={() => router.push(`/routes/employee?id=${single.id}`)}
            >
              {single.FirstName} 
{' '}
{single.LastName}
            </Button>
          ))}
      </ul>
    </Dashboard>
  );
}

export async function getServerSideProps() {
  try {
    const employees = await getAllEmployees();
    return { props: { employees } };
  } catch (e) {
    return false;
  }
}
