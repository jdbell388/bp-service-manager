import React from 'react';
import { Button } from '@chakra-ui/react';
import { withAuthSync, signOut } from '../config/auth';
import { Dashboard } from '../components/view';

function dashboard() {
  return (
    <Dashboard title="Dashboard">
      <Button onClick={signOut} bg="tomato" color="white">
        Sign Out
      </Button>
    </Dashboard>
  );
}

export default withAuthSync(dashboard);
