import React from 'react';
import { Flex, Box, Heading, Button } from '@chakra-ui/react';
import { Sidebar, Header } from '.';

const Content = ({ children }: any) => (
  <Box boxShadow="md" flex="1">
    {children}
  </Box>
);

export default function Dashboard({ children, title, backButton }: any) {
  return (
    <Flex w="100%" h="100vh">
      <Sidebar />
      <Content>
        <Header backButton={backButton} />
        <Box p="70px">
          {title && (
            <Heading as="h2" size="xl" mb="30px" textTransform="capitalize">
              {title}
            </Heading>
          )}
          {children}
        </Box>
      </Content>
    </Flex>
  );
}
