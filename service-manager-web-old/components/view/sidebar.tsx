import React from 'react';
import { Box, VStack, Link, Icon, Heading, Flex } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { AiOutlineHome, AiOutlineTeam } from 'react-icons/ai';
import { BiTask } from 'react-icons/bi';
import { FaRoute } from 'react-icons/fa';
import { MdPool } from 'react-icons/md';
import Image from 'next/image';

const ListItem = ({ title, link, icon }: any) => {
  const router = useRouter();
  const isActive = router.pathname === link;
  return (
    <Link
      href={link}
      w="100%"
      p="20px"
      _hover={{ background: 'white', boxShadow: 'lg', color: 'black' }}
      bg={isActive ? 'brand.m' : ''}
      color={isActive ? 'white' : 'black'}
      rounded="md"
      boxShadow={isActive ? 'lg' : ''}
    >
      {icon} 
{' '}
{title}{' '}
    </Link>
  );
};

export default function Sidebar() {
  const size = '7';
  return (
    <Box bg="gray.50" p="20px">
      <Flex
        boxShadow="sm"
        p="20px"
        rounded="md"
        bg="white"
        mb="30px"
        alignItems="center"
      >
        <Image src="/img/blue-pools-icon.png" width={40} height={44} />
        <Heading as="h1" size="md" pl="10px">
          Service Manager
        </Heading>
      </Flex>
      <VStack>
        <ListItem
          link="/"
          title="Dashboard"
          icon={<Icon as={AiOutlineHome} boxSize={size} />}
        />
        <ListItem
          link="/customers"
          title="Customers"
          icon={<Icon as={MdPool} boxSize={size} />}
        />
        <ListItem
          link="/employees"
          title="Employees"
          icon={<Icon as={AiOutlineTeam} boxSize={size} />}
        />
        <ListItem
          link="/routes"
          title="Routes"
          icon={<Icon as={FaRoute} boxSize={size} />}
        />
        <ListItem
          link="/workorders"
          title="Workorders"
          icon={<Icon as={BiTask} boxSize={size} />}
        />
      </VStack>
    </Box>
  );
}
