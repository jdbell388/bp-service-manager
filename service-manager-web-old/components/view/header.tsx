import React from 'react';
import { IconButton, Flex } from '@chakra-ui/react';
import { ImExit } from 'react-icons/im';

const Header = ({ backButton }: any) => {
  const back = backButton || <span />;
  return (
    <Flex
      bg="brand.vd"
      color="white"
      boxShadow="md"
      width="100%"
      p="5px 10px"
      justifyContent="space-between"
      alignContent="center"
      alignItems="center"
    >
      {back}
      <IconButton
        color="white"
        bg="transparent"
        aria-label="logout"
        fontSize="25px"
        icon={<ImExit />}
      />
    </Flex>
  );
};

export default Header;
