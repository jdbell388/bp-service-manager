import React from 'react';
import { useRouter } from 'next/router';
import { useEmployeeRoutesByDay } from '../Route-hooks';

export default function EmployeesRoutesByDay({ id, day, setDay }: any) {
  const router = useRouter();
  const [routes] = useEmployeeRoutesByDay(id, day);
  return (
    <>
      {routes.isLoading && <h1>Loading</h1>}
      {routes.data && (
        <>
          <button onClick={() => setDay('')} type="button">
            Back
          </button>

          <ol>
            {routes.data?.route.length &&
              routes.data.route.map((singleRouterow: any, index: any) => {
                return (
                  <li>
                    {singleRouterow.customerName}
                    <span>{singleRouterow.customerAddress}</span>
                  </li>
                );
              })}
          </ol>
          <button
            type="button"
            onClick={() => router.push(`/routes/edit?id=${routes.data.id}`)}
          >
            Edit
          </button>
        </>
      )}
    </>
  );
}
