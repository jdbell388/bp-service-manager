import React, { useState } from 'react';
import { Field } from 'formik';
import { DeleteIcon } from '@chakra-ui/icons';
import { Input, Button, Heading, Box, Flex } from '@chakra-ui/react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

const reorder = (list: any, startIndex: any, endIndex: any) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const Single = ({ values, index, handleChange, arrayHelpers }: any) => {
  return (
    <Flex
      key={values}
      justify="space-between"
      alignContent="center"
      alignItems="center"
      mb="10px"
    >
      <Box>
        <Heading as="h4" size="md">
          {`${index + 1}.`}
        </Heading>
      </Box>
      <Box w="43%">
        <Field
          name={`customers[${index}].customerName`}
          render={() => (
            <Input
              type="text"
              value={values.customerName}
              onChange={handleChange}
              placeholder="Name"
              name={`customers[${index}].customerName`}
            />
          )}
        />
      </Box>
      <Box w="43%">
        <Field
          name={`customers.${index}.customerAddress`}
          render={() => (
            <Input
              type="text"
              value={values.customerAddress}
              onChange={handleChange}
              placeholder="Address"
              name={`customers.${index}.customerAddress`}
            />
          )}
        />
      </Box>
      <Box>
        <Button
          type="button"
          bg="Tomato"
          color="white"
          onClick={() => arrayHelpers.remove(index)}
        >
          <DeleteIcon boxSize="3" mr="5px" /> Remove Stop
        </Button>
      </Box>
    </Flex>
  );
};

export const StopList = ({ values, handleChange, arrayHelpers }: any) => {
  const [state, setState] = useState(values);

  function onDragEnd(result: any) {
    if (!result.destination) {
      return;
    }

    if (result.destination.index === result.source.index) {
      return;
    }

    const quotes = reorder(
      state.quotes,
      result.source.index,
      result.destination.index
    );

    setState({ quotes });
  }
  console.log('values', values);
  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId="list">
        {(provided: any) => (
          <div ref={provided.innerRef} {...provided.droppableProps}>
            {values.customers.map((customer: any, index: number) => (
              <Draggable draggableId={customer.id} index={index}>
                {(dragProvided: any) => (
                  <div
                    ref={dragProvided.innerRef}
                    {...dragProvided.draggableProps}
                    {...dragProvided.dragHandleProps}
                  >
                    <Single
                      values={customer}
                      index={index}
                      handleChange={handleChange}
                      arrayHelpers={arrayHelpers}
                    />
                  </div>
                )}
              </Draggable>
            ))}
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </DragDropContext>
  );
};
