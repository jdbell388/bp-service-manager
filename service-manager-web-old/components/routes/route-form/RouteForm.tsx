import React, { useState } from 'react';
import { AddIcon, DeleteIcon, DragHandleIcon } from '@chakra-ui/icons';
import { Input, Button, Select, Heading, Box, Flex } from '@chakra-ui/react';
import { Container, Draggable } from 'react-smooth-dnd';
import arrayMove from 'array-move';
import useForm from '../../form/hooks/Form-hooks';

// Todo: Import actual customers

export default function RouteForm({
  submitRouteForm,
  editRoute,
  removeRoute,
}: any) {
  const initialValues = editRoute
    ? { day: editRoute.day, customers: editRoute.route }
    : { day: '', customers: [] };
  const [values, handleUpdate, updateSingleVal] = useForm({
    day: initialValues?.day ? initialValues?.day : '',
    customers: initialValues?.customers
      ? initialValues?.customers
      : [{ name: '', address: '' }],
  });

  const [stops, setStops] = useState(values.customers);
  const removeStop = (index: number) => {
    const newStops = stops.splice(index, 1);
    setStops(newStops);
  };
  const addStop = (vals: any) => {
    setStops([...stops, vals]);
  };

  const onDrop = ({ removedIndex, addedIndex }: any) => {
    if (typeof stops !== 'undefined') {
      console.log(removedIndex, addedIndex);
      setStops(() => arrayMove(stops, removedIndex, addedIndex));
    }
  };

  function getRandomInt(min: number, max: number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  const customHandleUpdate = (event: any) => {
    const { target } = event;
    const id = target.id.split('.');

    updateSingleVal({
      ...values[id[0]],
      [id[1]]: {
        ...values[id[0]][id[1]],
        [id[2]]: target.value
      }
    }, id[0]);
  }

  return (
    <form>
      <Box mb="30px">
        <label htmlFor="day">Day</label>
        <Select
          name="day"
          id="day"
          onChange={handleUpdate}
          value={values.day}
          placeholder="-- Select Day --"
        >
          <option value="monday">Monday</option>
          <option value="tuesday">Tuesday</option>
          <option value="wednesday">Wednesday</option>
          <option value="thursday">Thursday</option>
          <option value="friday">Friday</option>
          <option value="saturday">Saturday</option>
        </Select>
      </Box>
      <Box mb="40px">
        <Heading as="h3" size="lg" mb="10px">
          Stops
        </Heading>
        <>
          <Container onDrop={onDrop}>
            {stops.map((customer: any, index: number) => (
              <Draggable key={customer.id + index}>
                <Flex
                  justify="space-between"
                  alignContent="center"
                  alignItems="center"
                  mb="10px"
                >
                  <Box>
                    <Heading as="h4" size="md">
                      {index + 1}
.
</Heading>
                  </Box>
                  <Box w="43%">
                    <Input
                      type="text"
                      value={customer.customerName}
                      onChange={customHandleUpdate}
                      placeholder="Name"
                      name={`customers.${index}.customerName`}
                      id={`customers.${index}.customerName`}
                    />
                  </Box>
                  <Box w="43%">
                    <Input
                      type="text"
                      value={customer.customerAddress}
                      onChange={customHandleUpdate}
                      placeholder="Address"
                      name={`customers.${index}.customerAddress`}
                      id={`customers.${index}.customerAddress`}
                    />
                  </Box>
                  <Box>
                    <Button
                      type="button"
                      bg="Tomato"
                      color="white"
                      onClick={() => removeStop(index)}
                    >
                      <DeleteIcon boxSize="3" mr="5px" />
                      {' '}
Remove Stop
</Button>
                  </Box>
                </Flex>
              </Draggable>
            ))}
          </Container>
          <Button
            type="button"
            bg="brand.m"
            color="white"
            mt="20px"
            onClick={() =>
              addStop({ name: '', address: '', id: getRandomInt(1, 10000) })
            }
          >
            <AddIcon boxSize="3" mr="5px" /> Add Stop
          </Button>
        </>
      </Box>
      <Button type="submit" bg="SeaGreen" color="white" mt="20px" onClick={(e) => {
        e.preventDefault();
        console.log(values);
        submitRouteForm(values);
      }}>
        Submit Route
      </Button>
      {typeof removeRoute === 'function' && (
        <Button
          type="button"
          bg="Tomato"
          color="white"
          onClick={() => removeRoute()}
        >
          Remove Route
        </Button>
      )}
    </form>
  );
}
