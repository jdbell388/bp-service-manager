import { useEffect, useReducer } from 'react';
import {
  getAllEmployeeRoutes,
  getAllEmployeeRoutesByDay,
  getSingleRoute
} from '../../firebase/routes';

const routeReducer = (state: any, action: any) => {
  switch (action.type) {
    case 'FETCH_INIT':
      return {
        ...state,
        isLoading: true,
        isError: false
      };
    case 'FETCH_SUCCESS':
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: action.payload
      };
    case 'FETCH_FAILURE':
      return { ...state, isLoading: false, isError: true };
    case 'NO_DATA':
      return { ...state, isLoading: false, isError: false };
    default:
      throw new Error();
  }
};

export function useEmployeeRoutes(id: string) {
  const [state, dispatch] = useReducer(routeReducer, {
    isLoading: false,
    isError: false,
    data: undefined
  });

  useEffect(() => {
    const init = async () => {
      dispatch({ type: 'FETCH_INIT' });
      try {
        const results = await getAllEmployeeRoutes(id);
        dispatch({ type: 'FETCH_SUCCESS', payload: results });
      } catch (e) {
        dispatch({ type: 'FETCH_FAILURE' });
      }
    };
    init();
  }, [id]);

  return [state];
}

export function useEmployeeRoutesByDay(id: string, day: string) {
  const [state, dispatch] = useReducer(routeReducer, {
    isLoading: false,
    isError: false,
    data: undefined
  });

  useEffect(() => {
    const init = async () => {
      if (!id || !day) {
        dispatch({ type: 'NO_DATA' });
        return;
      }
      dispatch({ type: 'FETCH_INIT' });
      try {
        const results = await getAllEmployeeRoutesByDay(id, day);
        dispatch({ type: 'FETCH_SUCCESS', payload: results });
      } catch (e) {
        dispatch({ type: 'FETCH_FAILURE' });
      }
    };
    init();
  }, [id]);

  const setRoutes = (routes: any) => {
    dispatch({ type: 'FETCH_SUCCESS', payload: routes });
  };

  return [state, setRoutes];
}

export function useSingleRoute(id: string) {
  const [state, dispatch] = useReducer(routeReducer, {
    isLoading: false,
    isError: false,
    data: undefined
  });

  useEffect(() => {
    const init = async () => {
      dispatch({ type: 'FETCH_INIT' });
      try {
        const results = await getSingleRoute(id);
        dispatch({ type: 'FETCH_SUCCESS', payload: results });
      } catch (e) {
        dispatch({ type: 'FETCH_FAILURE' });
      }
    };
    init();
  }, [id]);

  return [state];
}
