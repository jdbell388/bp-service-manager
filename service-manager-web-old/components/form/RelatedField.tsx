import React, { useState } from 'react';
import { Select } from '@chakra-ui/react';

interface Props {
  label: string;
  onChange: any;
  posts: any;
  initialValue: any;
}

function RelatedField({ posts, onChange, label, initialValue }: Props) {
  const [selected, setSelected] = useState();

  const handleChange = (e: any) => {
    const { selectedIndex } = e.target.options;
    const id = e.target.options[selectedIndex].getAttribute('data-id');
    setSelected(e.target.value);
    onChange({ name: e.target.value, id } as any);
  };
  return (
    <fieldset>
      <label htmlFor={label}>{label}</label>
      {posts && (
        <Select
          onChange={handleChange}
          value={selected}
          name={label}
          placeholder={`Select ${label}`}
          mb="10px"
        >
          {posts.map((single: any) => {
            const isSelected = initialValue.id === single.id;
            return (
              <option
                value={single.name}
                key={single.id}
                data-id={single.id}
                selected={isSelected}
              >
                {single.name}
              </option>
            );
          })}
        </Select>
      )}
    </fieldset>
  );
}

export default RelatedField;
