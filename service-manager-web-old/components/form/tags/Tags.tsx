import React, { useState } from 'react';

const Tags = ({ values, updateForm }: any) => {
  const [tags, setTags] = useState(values);
  const [singleTag, setSingleTag] = useState<any>(null);

  const handleUpdate = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { target } = e;
    setSingleTag(target.value);
  };

  const addTag = (e: any) => {
    e.preventDefault();
    if (tags.length > 0) {
      setTags([...tags, singleTag]);
      updateForm(tags, 'tags');
    } else {
      setTags([singleTag]);
    }
    setSingleTag('');
  };

  const removeTag = (tag: string) => {
    console.log(`remove tag ${tag}`);
    const newTags = tags.filter((single: any) => {
      if (single !== tag) {
        return true;
      }
      return false;
    });
    setTags(newTags);
    return true;
  };

  return (
    <>
      <p>
        Tags:
        {tags.length > 0 &&
          tags.map((tag: string) => {
            return (
              <span className="single-tag" key={tag}>
                {tag}
                <button type="button" onClick={(e: any) => removeTag(tag)}>
                  X
                </button>
              </span>
            );
          })}
      </p>
      <label htmlFor="tags">Tags</label>
      <input
        type="text"
        name="tags"
        id="tags"
        value={singleTag}
        onChange={handleUpdate}
      />
      <button type="submit" onClick={addTag}>
        Add
      </button>
    </>
  );
};

export default Tags;
