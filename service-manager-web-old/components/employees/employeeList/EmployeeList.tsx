import React, { useMemo } from 'react';
import Link from 'next/link';
import { ListTable } from '../../tables';
import { EmployeeInt } from '../../../firebase/employees';

export default function EmployeeList({ employees }: any) {
  const data = useMemo(
    () =>
      employees.map((employee: EmployeeInt) => ({
        name: `${employee.FirstName} ${employee.LastName}`,
        address: employee.Address,
        phone: employee.Phone,
        link: `/employees/employee?id=${employee.id}`,
      })),
    []
  );
  const columns = useMemo(
    () => [
      {
        Header: 'Name',
        accessor: 'name',
      },
      {
        Header: 'Address',
        accessor: 'address',
      },
      {
        Header: 'Phone',
        accessor: 'phone',
      },
      {
        Header: 'Link',
        accessor: 'link',
        Cell: (row: any) => <Link href={row.value}>Details</Link>,
      },
    ],
    []
  );
  return (
    <>
      {employees ? (
        <ListTable columns={columns} data={data} />
      ) : (
        <p>No Employees Found</p>
      )}
    </>
  );
}
