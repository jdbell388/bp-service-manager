import { useState, useEffect } from 'react';
import { getAllEmployees, EmployeeInt } from '../../../firebase/employees';

export default function useEmployees() {
  const [employees, setEmployees] = useState<EmployeeInt[]>();
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    const getInitialProps = async () => {
      setLoading(true);
      try {
        const results = await getAllEmployees();
        setEmployees(results);
        setLoading(false);
      } catch (e) {
        setLoading(false);
        setError(true);
      }
    };
    getInitialProps();
  }, []);

  const result: [EmployeeInt[] | undefined, boolean, boolean] = [
    employees,
    loading,
    error
  ];
  return result;
}
