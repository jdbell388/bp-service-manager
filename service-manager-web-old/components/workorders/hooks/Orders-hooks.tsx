import { useEffect, useReducer } from 'react';
import { getSingleOrder } from '../../../firebase/workorders';

export default function useSingleOrder(id: string) {
  const singleOrderReducer = (state: any, action: any) => {
    switch (action.type) {
      case 'FETCH_INIT':
        return {
          ...state,
          isLoading: true,
          isError: false
        };
      case 'FETCH_SUCCESS':
        return {
          ...state,
          isLoading: false,
          isError: false,
          data: action.payload
        };
      case 'FETCH_FAILURE':
        return { ...state, isLoading: false, isError: true };
      default:
        throw new Error();
    }
  };
  const [state, dispatch] = useReducer(singleOrderReducer, {
    isLoading: false,
    isError: false,
    order: undefined
  });

  useEffect(() => {
    const getInitialProps = async () => {
      dispatch({ type: 'FETCH_INIT' });
      try {
        const results = await getSingleOrder(id);
        dispatch({ type: 'FETCH_SUCCESS', payload: results });
      } catch (e) {
        dispatch({ type: 'FETCH_FAILURE' });
      }
    };
    getInitialProps();
  }, [id]);

  return [state];
}
