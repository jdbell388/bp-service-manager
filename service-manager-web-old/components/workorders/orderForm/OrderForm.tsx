import React, { useState, useEffect } from 'react';
import * as R from 'rambda';
import { Input, Button, Textarea } from '@chakra-ui/react';
import useForm from '../../form/hooks/Form-hooks';

import { CustomerInt } from '../../../firebase/customers';
import { EmployeeInt } from '../../../firebase/employees';
import { getAllObj } from '../../../firebase/utils';
import RelatedField from '../../form/RelatedField';

function OrderForm(props: any) {
  const { submit, order } = props;
  const [accounts, setAccounts] = useState<any>();
  const [Employees, setEmployees] = useState<any>();
  const [values, handleUpdate, updateSingleVal] = useForm({
    Title: order ? order.Title : '',
    Description: order ? order.Description : '',
    Employee: order ? order.Employee : '',
    account: order ? order.account : '',
    tags: order ? order.tags : '',
  });

  const getEmployees = async () => {
    const results = await getAllObj('employees');
    const predicate = (single: any) => {
      const obj = {
        id: single.id,
        name: `${single.data.FirstName} ${single.data.LastName}`,
      };
      return obj;
    };
    const returnArr = await R.map(predicate, results);
    setEmployees(returnArr);
  };

  const getAccounts = async () => {
    const results = await getAllObj('accounts');
    const predicate = (single: any) => {
      const obj = {
        id: single.id,
        name: `${single.data.FirstName} ${single.data.LastName}`,
      };
      return obj;
    };
    const returnArr = await R.map(predicate, results);
    setAccounts(returnArr);
  };

  useEffect(() => {
    getAccounts();
    getEmployees();
  }, []);

  return (
    <form>
      <div>
        <label htmlFor="Title">Title</label>
        <Input
          type="text"
          name="Title"
          id="Title"
          placeholder="Title"
          value={values.Title}
          onChange={handleUpdate}
          mb="10px"
        />
        <label htmlFor="Description">Description</label>
        <Textarea
          name="Description"
          placeholder="Description"
          id="Description"
          value={values.Description}
          onChange={handleUpdate}
          mb="10px"
        />
        {accounts && (
          <RelatedField
            posts={accounts}
            label="Account"
            initialValue={values.account}
            onChange={(val: CustomerInt) => {
              updateSingleVal(val, 'account');
            }}
          />
        )}
        {Employees && (
          <RelatedField
            posts={Employees}
            label="Employee"
            initialValue="Select Employee"
            onChange={(val: EmployeeInt) => {
              updateSingleVal(val, 'Employee');
            }}
          />
        )}

        <div>
          <Button
            type="button"
            bg="SeaGreen"
            color="white"
            mt="20px"
            onClick={() =>
              submit(
                values.Title,
                values.Description,
                values.Employee,
                values.account,
                values.tags
              )}
          >
            Submit
          </Button>
        </div>
      </div>
    </form>
  );
}

export default OrderForm;
