import React, { useMemo } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import Link from 'next/link';
import { ListTable } from '../../tables';
import { CustomerInt } from '../../../firebase/customers';

export default function CustomerList({ customers }: any) {
  const data = useMemo(
    () =>
      customers.map((customer: CustomerInt) => ({
        name: `${customer.FirstName} ${customer.LastName}`,
        address: customer.Address,
        employee: customer.Employee?.name,
        link: `/customers/customer?id=${customer.id}`,
      })),
    []
  );
  const columns = useMemo(
    () => [
      {
        Header: 'Name',
        accessor: 'name',
      },
      {
        Header: 'Address',
        accessor: 'address',
      },
      {
        Header: 'Employee',
        accessor: 'employee',
      },
      {
        Header: 'Link',
        accessor: 'link',
        Cell: (row: any) => <Link href={row.value}>Details</Link>,
      },
    ],
    []
  );
  return (
    <AnimatePresence>
      {customers ? (
        <motion.div
          initial={{ opacity: 0, y: 50 }}
          animate={{ opacity: 1, y: 0 }}
          exit={{ opacity: 0, y: 50 }}
          transition={{ duration: 0.5, delay: 0.3 }}
        >
          <ListTable columns={columns} data={data} />
        </motion.div>
      ) : (
        <h1>No Customers</h1>
      )}
    </AnimatePresence>
  );
}
