import { useState, useEffect } from 'react';
import { getAllCustomers, CustomerInt } from '../../../firebase/customers';

export default function useCustomers() {
  const [customers, setCustomers] = useState<CustomerInt[]>();
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    (async () => {
      setLoading(true);
      try {
        const results = await getAllCustomers();
        setCustomers(results);
        setLoading(false);
      } catch (e) {
        setLoading(false);
        setError(true);
      }
    })();
  }, []);
  const result: [
    CustomerInt[] | undefined,
    React.Dispatch<React.SetStateAction<CustomerInt[] | undefined>>,
    boolean,
    boolean
  ] = [customers, setCustomers, loading, error];
  return result;
}
