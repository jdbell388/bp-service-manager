import React from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import { Button, Heading, Link } from '@chakra-ui/react';
import { DeleteIcon, SettingsIcon } from '@chakra-ui/icons';
import { useRouter } from 'next/router';
import { deleteCustomer } from '../../../firebase/customers';

export default function CustomerSingle({ customer, id, title }: any) {
  const router = useRouter();

  const removeCustomer = async () => {
    await deleteCustomer(id);
    alert('customer Deleted');
    router.push('/customers');
  };

  return (
    <AnimatePresence>
      {customer ? (
        <motion.div
          initial={{ opacity: 0, y: 50 }}
          animate={{ opacity: 1, y: 0 }}
          exit={{ opacity: 0, y: 50 }}
          transition={{ duration: 0.5, delay: 0.3 }}
        >
          <div>
            {customer.FirstName && (
              <p>
                <strong>First Name: </strong>
                {customer.FirstName}
              </p>
            )}
            {customer.LastName && (
              <p>
                <strong>Last Name: </strong>
                {customer.LastName}
              </p>
            )}
            {customer.Address && (
              <p>
                <strong>Address: </strong> 
{' '}
{customer.Address}
              </p>
            )}
            {customer.Employee && (
              <p>
                <strong>Employee: </strong> {customer.Employee?.name}
              </p>
            )}
            {customer.Phone && (
              <p>
                <strong>Phone: </strong> 
{' '}
{customer.Phone}
              </p>
            )}
            {customer.Email && (
              <p>
                <strong>Email: </strong> {customer.Email}
              </p>
            )}
            <Button
              p="10px"
              rounded="md"
              bg="SlateGray"
              color="AliceBlue"
              d="inline-block"
              mt="20px"
              mr="20px"
              boxShadow="md"
              type="button"
              onClick={() => {
                router.push(`/customers/edit?id=${id}`);
              }}
            >
              <SettingsIcon boxSize="3" /> Edit
            </Button>
            <Button
              p="10px"
              rounded="md"
              bg="Tomato"
              color="white"
              d="inline-block"
              mt="20px"
              boxShadow="md"
              type="button"
              onClick={removeCustomer}
            >
              <DeleteIcon boxSize="3" /> Delete
            </Button>
          </div>
        </motion.div>
      ) : (
        <p>Loading...</p>
      )}
    </AnimatePresence>
  );
}
