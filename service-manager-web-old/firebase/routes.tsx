import firebase from './config';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/storage';

export interface RouteSingleInt {
  customerAddress: string;
  customerName: string;
}

export interface RouteInt {
  id?: string;
  day?: string;
  employeeId?: string;
  employeeName?: string;
  route?: RouteSingleInt[];
}

export const addRoute = async ({
  day,
  employeeId,
  employeeName,
  route,
}: RouteInt) => {
  const db = firebase.firestore();
  try {
    const addDoc = await db
      .collection('routes')
      .doc(`${employeeId}_${day}`)
      .set({ day, employeeId, employeeName, route });
    return addDoc;
  } catch (e) {
    console.error('Error: ', e);
  }
  return {};
};

export const getAllEmployeeRoutes = async (id: string) => {
  const db = firebase.firestore();
  try {
    const routeList = await db
      .collection('routes')
      .where('employeeId', '==', id)
      .get();
    const results: RouteInt[] = [];
    routeList.forEach((doc) => {
      const data = doc.data();
      results.push({ id: doc.id, ...data });
    });
    return results;
  } catch (e) {
    console.error('Error getting routes: ', e);
  }
  return [];
};

export const getAllEmployeeRoutesByDay = async (id: string, day: string) => {
  const db = firebase.firestore();
  try {
    const route = await db
      .collection('routes')
      .where('employeeId', '==', id)
      .where('day', '==', day)
      .limit(1)
      .get();

    let result = {};
    route.forEach((doc) => {
      const data = doc.data();
      result = { id: doc.id, ...data };
    });
    return result;
  } catch (e) {
    console.error('Error getting routes: ', e);
  }
  return {};
};

export const getSingleRoute = async (id: string) => {
  const db = firebase.firestore();
  try {
    const route = await db.collection('routes').doc(id).get();

    const returnData: RouteInt | undefined = route.data();
    return returnData;
  } catch (e) {
    console.error('Error getting route: ', e);
  }
  return {};
};

export const deleteRoute = async (id: string) => {
  const db = firebase.firestore();
  try {
    await db.collection('routes').doc(id).delete();
    return 200;
  } catch (e) {
    console.error('Error deleting route: ', e);
  }
  return [];
};

export const updateRoute = async ({
  day,
  employeeId,
  employeeName,
  route,
  id,
}: any) => {
  const db = firebase.firestore();

  try {
    const ref = await db.collection('routes').doc(id);
    const updateDoc = await ref.update({
      day,
      employeeId,
      employeeName,
      route,
    });
    return updateDoc;
  } catch (e) {
    console.error('Error: ', e);
  }
  return {};
};
