import firebase from './config';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/storage';

interface CustomerEmployee {
  id: string;
  name: string;
}

export interface CustomerInt {
  Address?: string;
  Email?: string;
  Employee?: CustomerEmployee;
  FirstName?: string;
  LastName?: string;
  Phone?: string;
  id?: any;
}

export const addCustomer = async ({
  FirstName,
  LastName,
  Address,
  Email,
  Employee,
  Phone,
}: CustomerInt) => {
  const db = firebase.firestore();
  try {
    const addDoc = await db
      .collection('accounts')
      .add({ FirstName, LastName, Address, Email, Employee, Phone });
    return addDoc;
  } catch (e) {
    console.error('Error: ', e);
  }
  return {};
};

export const getAllCustomers = async () => {
  const db = firebase.firestore();
  try {
    const customerList = await db.collection('accounts').get();
    const results: CustomerInt[] = [];
    customerList.forEach((doc) => {
      const data = doc.data();
      results.push({ id: doc.id, ...data });
    });
    return results;
  } catch (e) {
    console.error('Error getting customers: ', e);
  }
  return [];
};

export const getSingleCustomer = async (id: string) => {
  const db = firebase.firestore();
  try {
    const customerList = await db.collection('accounts').doc(id).get();

    const customerListData: CustomerInt | undefined = customerList.data();
    return customerListData;
  } catch (e) {
    console.error('Error getting customer: ', e);
  }
  return {};
};

export const deleteCustomer = async (id: string) => {
  const db = firebase.firestore();
  try {
    await db.collection('accounts').doc(id).delete();
    return 200;
  } catch (e) {
    console.error('Error deleting customer: ', e);
  }
  return [];
};

export const updateCustomer = async ({
  FirstName,
  LastName,
  Address,
  Email,
  Employee,
  Phone,
  id,
}: CustomerInt) => {
  const db = firebase.firestore();

  try {
    const customerRef = await db.collection('accounts').doc(id);
    const updateDoc = await customerRef.update({
      FirstName,
      LastName,
      Address,
      Email,
      Employee,
      Phone,
    });
    return updateDoc;
  } catch (e) {
    console.error('Error: ', e);
  }
  return {};
};
