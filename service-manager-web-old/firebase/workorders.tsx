import firebase from './config';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/storage';

interface CustomerEmployee {
  id: string;
  name: string;
}

interface OrderAcct {
  id: string;
  name: string;
}

export interface OrderInt {
  id?: any;
  Title?: string;
  Description?: string;
  Employee?: CustomerEmployee;
  account?: OrderAcct;
  Status?: string;
  tags?: [];
}

export const addOrder = async ({
  Title,
  Description,
  Employee,
  account,
  tags,
}: OrderInt) => {
  const db = firebase.firestore();
  try {
    const addDoc = await db.collection('orders').add({
      Title,
      Description,
      Employee,
      account,
      tags,
      Status: 'In Progress',
      Comments: '',
    });
    return addDoc;
  } catch (e) {
    console.error('Error: ', e);
  }
  return {};
};

export const getAllOrders = async () => {
  const db = firebase.firestore();
  try {
    const orderList = await db.collection('orders').get();
    const results: OrderInt[] = [];
    orderList.forEach((doc) => {
      const data = doc.data();
      results.push({ id: doc.id, ...data });
    });
    return results;
  } catch (e) {
    console.log('Error getting orders: ', e);
  }
  return [];
};

export const getSingleOrder = async (id: string) => {
  const db = firebase.firestore();
  try {
    const orderList = await db.collection('orders').doc(id).get();

    const orderListData: OrderInt | undefined = orderList.data();
    return orderListData;
  } catch (e) {
    console.error('Error getting order: ', e);
  }
  return {};
};

export const deleteOrder = async (id: string) => {
  const db = firebase.firestore();
  try {
    await db.collection('orders').doc(id).delete();
    return 200;
  } catch (e) {
    console.error('Error deleting order: ', e);
  }
  return [];
};

export const updateOrder = async ({
  Title,
  Description,
  Employee,
  account,
  tags,
  id,
}: OrderInt) => {
  const db = firebase.firestore();

  try {
    const orderRef = db.collection('orders').doc(id);
    const updateDoc = await orderRef.update({
      Title,
      Description,
      Employee,
      account,
      tags,
      Status: 'In Progress',
      Comments: '',
    });
    return updateDoc;
  } catch (e) {
    console.error('Error: ', e);
  }
  return {};
};

export const markComplete = async (id: string) => {
  const db = firebase.firestore();

  try {
    const orderRef = db.collection('orders').doc(id);
    const updateDoc = await orderRef.update({
      Status: 'Complete',
    });
    return updateDoc;
  } catch (e) {
    console.error('Error: ', e);
  }
  return {};
};
