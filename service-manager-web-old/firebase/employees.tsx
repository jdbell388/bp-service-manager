import firebase from './config';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/storage';

export interface EmployeeInt {
  id?: any;
  FirstName?: string;
  LastName?: string;
  Address?: string;
  Email?: string;
  Phone?: string;
}

export const addEmployee = async ({
  FirstName,
  LastName,
  Address,
  Email,
  Phone,
}: EmployeeInt) => {
  const db = firebase.firestore();
  try {
    const addDoc = await db
      .collection('employees')
      .add({ FirstName, LastName, Address, Email, Phone });
    return addDoc;
  } catch (e) {
    console.error('Error: ', e);
  }
  return {};
};

export const getAllEmployees = async () => {
  const db = firebase.firestore();
  try {
    const employeeList = await db.collection('employees').get();
    const results: EmployeeInt[] = [];
    employeeList.forEach((doc) => {
      const data = doc.data();
      results.push({ id: doc.id, ...data });
    });
    return results;
  } catch (e) {
    console.error('Error getting employees: ', e);
  }
  return [];
};

export const getSingleEmployee = async (id: string) => {
  const db = firebase.firestore();
  try {
    const employeeList = await db.collection('employees').doc(id).get();

    const employeeListData: EmployeeInt | undefined = employeeList.data();
    return employeeListData;
  } catch (e) {
    console.error('Error getting employee: ', e);
  }
  return {};
};

export const deleteEmployee = async (id: string) => {
  const db = firebase.firestore();
  try {
    await db.collection('employees').doc(id).delete();
    return 200;
  } catch (e) {
    console.error('Error deleting employee: ', e);
  }
  return [];
};

export const updateEmployee = async ({
  FirstName,
  LastName,
  Address,
  Email,
  Phone,
  id,
}: EmployeeInt) => {
  const db = firebase.firestore();

  try {
    const employeeRef = await db.collection('employees').doc(id);
    const updateDoc = await employeeRef.update({
      FirstName,
      LastName,
      Address,
      Email,
      Phone,
    });
    return updateDoc;
  } catch (e) {
    console.error('Error: ', e);
  }
  return {};
};
