import firebase from "./config";
import "firebase/firestore";
import "firebase/auth";
import "firebase/storage";

export const getAllObj = async (objectType: string) => {
  const db = firebase.firestore();
  try {
    const clientList = await db.collection(objectType).get();
    const results: {}[] = [];
    clientList.forEach(doc => {
      results.push({ id: doc.id, data: doc.data() });
    });
    return results;
  } catch (e) {
    console.log(`Error getting ${objectType}: `, e);
  }
  return [];
};
