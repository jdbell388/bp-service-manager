import { collection, doc, setDoc, getDocs, getDoc, deleteDoc, query, where, limit } from "firebase/firestore"; 
import { database } from '../firebase/config';

export interface ReportInt {
  id?: any;
  address?: string;
  chlAdded?: string;
  chlorine?: string;
  customer?: string;
  ph?: string;
  phAdded?: string;
  ta?: string;
  taAdded?: string;
  time?: string;
}

export const addReport = async ({
  address,
  chlAdded,
  chlorine,
  customer,
  ph,
  phAdded,
  ta,
  taAdded,
  time,
}: ReportInt) => {
  const reportsRef = collection(database, 'reports');
  try {
    const addDoc = await setDoc(doc(reportsRef), { address,
      chlAdded,
      chlorine,
      customer,
      ph,
      phAdded,
      ta,
      taAdded,
      time });
    return addDoc;
  } catch (e) {
    console.error('Error: ', e);
  }
  return {};
};

export const getAllReports = async () => {
  try {
    const reportList = await getDocs(collection(database, 'reports'));
    const results: ReportInt[] = [];
    reportList.forEach((doc) => {
      const data = doc.data();
      const { address, chlAdded, chlorine, customer, ph, phAdded, ta, taAdded } = data;
      results.push({address, chlAdded, chlorine, customer, ph, phAdded, ta, taAdded});
    });
    return results;
  } catch (e) {
    console.error('Error getting reports: ', e);
  }
  return [];
};

export const getSingleReport = async (id: string) => {
  const docRef = doc(database, "reports", id);
  try {
    const reportList = await getDoc(docRef);
    return reportList.data();
  } catch (e) {
    console.error('Error getting report: ', e);
  }
  return {};
};

export const deleteReport = async (id: string) => {
  try {
    await deleteDoc(doc(database, 'reports', id));
    return 200;
  } catch (e) {
    console.error('Error deleting report: ', e);
  }
  return [];
};

export const updateReport = async ({
  address,
  chlAdded,
  chlorine,
  customer,
  ph,
  phAdded,
  ta,
  taAdded,
  time,
}: ReportInt, {id}: any) => {
  try {
    const updateDoc = await setDoc(doc(database, 'reports', id), {
      address,
      chlAdded,
      chlorine,
      customer,
      ph,
      phAdded,
      ta,
      taAdded,
      time,
    });
    return updateDoc;
  } catch (e) {
    console.error('Error: ', e);
  }
  return {};
};

export const getRelatedAddresses = async (employeeId: string) => {
  console.log('employeeId', typeof employeeId);
  console.log('test', typeof 'M0nzwO4syLFjYnzuQJJB')
  const ref = collection(database, "accounts");
  const whereIs = where('EmployeeId', '==', `${employeeId}`);
  const q = query(ref, whereIs);
  try {
    const route = await getDocs(q);

    let result = [];
    route.forEach((doc) => {
      const data = doc.data();
      result.push({ id: doc.id, ...data });
    });
    return result;
  } catch (e) {
    console.error('Error getting employee: ', e);
  }
}