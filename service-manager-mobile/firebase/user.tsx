import { collection, getDocs,query, where } from "firebase/firestore"; 
import { database } from './config';

export const getUserMetaByEmail = async (email: string) => {
  const ref = collection(database, "employees");
  const q = query(ref, where('Email', '==', email));
  try {
    const route = await getDocs(q);

    let result = {};
    route.forEach((doc) => {
      const data = doc.data();
      result = { id: doc.id, ...data };
    });
    return result;
  } catch (e) {
    console.error('Error getting employee: ', e);
  }
}