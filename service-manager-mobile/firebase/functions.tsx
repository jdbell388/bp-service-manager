import { collection, doc, setDoc, getDocs, getDoc, deleteDoc } from "firebase/firestore"; 
import { database } from '../firebase/config';

export const addNewObj = async (obj: any, type: string) => {
  const objRef = collection(database, type);
  try {
    await setDoc(doc(objRef), obj);
    return `New ${type} added successfully!`;
  } catch (e) {
    console.log('Error: ', e);
    return `Error: ${e}`;
  }
};

export const getAllObj = async (objectType: string) => {

  try {
    const objectList = await getDocs(collection(database, 'accounts'));
    const results: {}[] = [];
    objectList.forEach(doc => {
      results.push({ id: doc.id, data: doc.data() });
    });
    return results;
  } catch (e) {
    console.log(`Error getting ${objectType}: `, e);
  }
  return [];
};

export const getSingleObj = async (type: any, id: any) => {
  const docRef = doc(database, type, id);
  try {
    const objList = await getDoc(docRef);
    return objList.data();
  } catch (e) {
    console.log(`Error getting ${type} id: ${id}: `, e);
  }
  return {};
};

export const deleteObj = async (type: string, id: string) => {
  try {
    await deleteDoc(doc(database, type, id));
    return 200;
  } catch (e) {
    console.log(`Error deleting ${collection}: id: ${id}: `, e);
  }
  return [];
};

export const updateObj = async (
  type: string,
  id: string,
  updated: any
) => {

  try {
    const updatedObject = await setDoc(doc(database, type, id), updated);
    return updatedObject;
  } catch (e) {
    console.log(`Error updating ${collection} id: ${id}: `, e);
  }
  return {};
};
