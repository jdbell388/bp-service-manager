import { collection, doc, setDoc, getDocs, getDoc, deleteDoc } from "firebase/firestore"; 
import { database } from '../firebase/config';

interface CustomerEmployee {
  id: string;
  name: string;
}

interface OrderAcct {
  id: string;
  name: string;
}

export interface OrderInt {
  id?: any;
  Title?: string;
  Description?: string;
  Employee?: CustomerEmployee;
  account?: OrderAcct;
  Status?: string;
  tags?: [];
}

export const addOrder = async ({
  Title,
  Description,
  Employee,
  account,
  tags,
}: OrderInt) => {
  const orderRef = collection(database, 'orders');
  try {
    const addDoc = await setDoc(doc(orderRef), {
      Title,
      Description,
      Employee,
      account,
      tags,
      Status: 'In Progress',
      Comments: '',
    });
    return addDoc;
  } catch (e) {
    console.error('Error: ', e);
  }
  return {};
};

export const getAllOrders = async () => {
  try {
    const orderList = await getDocs(collection(database, 'orders'));
    const results: OrderInt[] = [];
    orderList.forEach((doc) => {
      const data = doc.data();
      results.push({ id: doc.id, ...data });
    });
    return results;
  } catch (e) {
    console.log('Error getting orders: ', e);
  }
  return [];
};

export const getSingleOrder = async (id: string) => {
  const docRef = doc(database, "orders", id);
  try {
    const orderList = await getDoc(docRef);

    const orderListData: OrderInt | undefined = orderList.data();
    return orderListData;
  } catch (e) {
    console.error('Error getting order: ', e);
  }
  return {};
};

export const deleteOrder = async (id: string) => {
  try {
    await deleteDoc(doc(database, 'orders', id));
    return 200;
  } catch (e) {
    console.error('Error deleting order: ', e);
  }
  return [];
};

export const updateOrder = async ({
  Title,
  Description,
  Employee,
  account,
  tags,
  id,
}: OrderInt) => {

  try {
    const updateDoc = setDoc(doc(database, 'orders', id), {
      Title,
      Description,
      Employee,
      account,
      tags,
      Status: 'In Progress',
      Comments: '',
    });
    return updateDoc;
  } catch (e) {
    console.error('Error: ', e);
  }
  return {};
};

export const markComplete = async (id: string) => {

  try {
    const updateDoc = setDoc(doc(database, 'orders', id), {
      Status: 'Complete',
    });
    return updateDoc;
  } catch (e) {
    console.error('Error: ', e);
  }
  return {};
};
