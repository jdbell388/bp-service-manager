import { collection, doc, setDoc, getDocs, getDoc, deleteDoc } from "firebase/firestore"; 
import { database } from '../firebase/config';

export const getAllObj = async (objectType: string) => {

  try {
    const objectList = await getDocs(collection(database, 'accounts'));
    const results: {}[] = [];
    objectList.forEach(doc => {
      results.push({ id: doc.id, data: doc.data() });
    });
    return results;
  } catch (e) {
    console.log(`Error getting ${objectType}: `, e);
  }
  return [];
};
