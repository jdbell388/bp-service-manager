import React, { useState, useEffect } from 'react';
import { View, Text, Button, TextInput } from 'react-native';
import RelatedField from '../form/RelatedField';

const CustomerForm = (props: any) => {
  const [FirstName, setFirstName] = useState<string>();
  const [LastName, setLastName] = useState<string>();
  const [Address, setAddress] = useState<string>();
  const [Employee, setEmployee] = useState<string>();
  const [Email, setEmail] = useState<string>();
  const [Phone, setPhone] = useState<string>();
  const { submit, customer } = props;

  const updateEmployee = async (newEmployee: any) => {
    await setEmployee(newEmployee);
  };

  useEffect(() => {
    if (customer) {
      setFirstName(customer.FirstName);
      setLastName(customer.LastName);
      setAddress(customer.Address);
      setEmployee(customer.Client);
      setEmail(customer.Email);
      setPhone(customer.Phone);
    }
  }, []);

  return (
    <View>
      <View>
        <TextInput
          placeholder="FirstName"
          value={FirstName}
          onChangeText={(text: any) => {
            setFirstName(text);
          }}
        />
      </View>
      <View>
        <TextInput
          placeholder="LastName"
          value={LastName}
          onChangeText={(text: any) => {
            setLastName(text);
          }}
        />
      </View>
      <View>
        <TextInput
          placeholder="Address"
          value={Address}
          onChangeText={(text: any) => {
            setAddress(text);
          }}
        />
      </View>
      <View>
        <TextInput
          placeholder="Email"
          value={Email}
          onChangeText={(text: any) => {
            setEmail(text);
          }}
        />
      </View>
      <View>
        <TextInput
          placeholder="Phone"
          value={Phone}
          onChangeText={(text: any) => {
            setPhone(text);
          }}
        />
      </View>
      <View>
        <RelatedField
          postType="employees"
          onChange={updateEmployee}
          initialValue={Employee}
        />
      </View>
      <View>
        <Button
          onPress={() => submit(FirstName,
            LastName,
            Address,
            Email,
            Employee,
            Phone)}
          title="Submit"
        />
      </View>
    </View>
  );
};

export default CustomerForm;
