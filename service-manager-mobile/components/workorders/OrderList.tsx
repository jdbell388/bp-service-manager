import React, { useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import { Icon } from 'react-native-elements';
import { OrderInt } from '../../firebase/customers';

export const Separator = () => <View style={styles.separator} />;

type Order = 'asc' | 'desc';

function complexSort(obj: any, orderBy: any, sortorder: any) {
  console.log('obj', obj);
  console.log('orderby', orderBy);
  console.log('sortorder', sortorder);
  if (sortorder === 'asc') {
    obj.sort((a: any, b: any) => (a[orderBy] > b[orderBy] ? 1 : -1));
  } else {
    obj.sort((a: any, b: any) => (b[orderBy] > a[orderBy] ? 1 : -1));
  }

  return obj;
}

const OrderList = ({ orders, navigation }: any) => {
  const [sortorder, setSortorder] = useState<Order>('asc');
  const [orderBy, setOrderBy] = useState<keyof OrderInt>('Title');

  return (
    <SafeAreaView>
      <ScrollView>
        {complexSort(orders, orderBy, sortorder).map((order: any) => {
          return (
            <>
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate('SingleOrder', {
                    id: order.Id,
                    order
                  })
                }
                style={styles.container}
              >
                <Text key={order.Id} style={styles.text}>
                  {order.Title}
                </Text>
                <Icon name="arrow-forward" style={{ fontSize: 20 }} />
              </TouchableOpacity>
              <Separator />
            </>
          );
        })}
      </ScrollView>
    </SafeAreaView>
  );
};

export default OrderList;

const styles = StyleSheet.create({
  container: {
    padding: 20,
    flexDirection: 'row',
    backgroundColor: '#fff',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  text: {
    fontSize: 18
  },
  separator: {
    height: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.2)'
  }
});
