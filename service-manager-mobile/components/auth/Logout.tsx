import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Button } from 'react-native-elements';
import { auth } from '../../firebase/config';
import { signOut } from 'firebase/auth';

export default function Logout({ navigation }: any) {
  const handleSubmit = async (e: any) => {
    e.preventDefault();
    try {
      await signOut(auth);
      await AsyncStorage.clear();
      navigation.navigate('Login');
    } catch (error) {
      alert(error);
    }
  };

  return (
    <Button
      onPress={() => handleSubmit}
      title="Logout"
      containerStyle={{ width: '100%', marginTop: 10, marginBottom: 10 }}
      buttonStyle={{ backgroundColor: '#3498db', height: 70, borderRadius: 10 }}
      titleStyle={{ fontWeight: 'bold', color: '#fff', fontSize: 24 }}
    />
  );
}
