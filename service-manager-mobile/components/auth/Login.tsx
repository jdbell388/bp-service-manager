import React, { useState } from 'react';
import { View, Button, TextInput } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { auth } from '../../firebase/config';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { getUserMetaByEmail } from '../../firebase/user';
import { EmployeeInt } from '../../firebase/employees';

export default function Login({ navigation }: any) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const handleSubmit = async (e: any) => {
    e.preventDefault();
    try {
      const userAuth = await signInWithEmailAndPassword(auth, email, password);
      await AsyncStorage.setItem('userToken', JSON.stringify(userAuth.user));
      const userMeta: EmployeeInt = await getUserMetaByEmail(email);
      await AsyncStorage.setItem('userId', JSON.stringify(userMeta.id));
      await AsyncStorage.setItem('userEmail', JSON.stringify(userMeta.Email));
      navigation.navigate('WorkOrders');
    } catch (e) {
      console.log(e);
      if (e.code === 'auth/invalid-email') {
        alert('You have entered an invalid Email address');
      }
      if (e.code === 'auth/wrong-password') {
        alert('Incorrect email or password. Please try again.');
      }
    }
  };
  return (
    <>
      <View>
        <TextInput
          onChangeText={change => setEmail(change)}
          placeholder="Email"
          value={email}
          textContentType="username"
        />
      </View>
      <View>
        <TextInput
          onChangeText={change => setPassword(change)}
          placeholder="Password"
          value={password}
          textContentType="password"
          secureTextEntry
        />
      </View>
      <View>
        <Button onPress={e => handleSubmit(e)} title="Login" />
      </View>
    </>
  );
}
