import React, { useState, useEffect } from 'react';
import { View, Text, Picker } from 'react-native';
import { getAllObj } from '../../firebase/utils';

interface Props {
  postType: string;
  onChange: any;
  initialValue?: any;
}

interface ObjObj {
  Id: string;
  Name: string;
}

const RelatedField = ({ postType, onChange, initialValue }: Props) => {
  const [objects, setObjects] = useState();
  const [selected, setSelected] = useState<ObjObj | {}>({});

  const getObjects = async () => {
    const results = await getAllObj(postType);
    const obj = results.map((singleResult: any) => {
      const result = { Name: '', Id: '' };
      if (singleResult.data?.FirstName) {
        result.Name = singleResult.data.FirstName;
      }
      if (singleResult.data?.LastName) {
        result.Name += ` ${singleResult.data.LastName}`;
      }
      result.Id = singleResult.id;
      return result;
    });
    setObjects(obj);
  };

  useEffect(() => {
    setSelected(initialValue);
    getObjects();
  }, []);

  return (
    <View>
      <Text>{postType}</Text>
      {objects && (
        <Picker
          selectedValue={selected}
          onValueChange={(val: number) => {
            setSelected(val);
            onChange(objects[val]);
            console.log('selected', selected);
          }}
        >
          <Picker.Item value="" label="-- Select --" />
          {objects &&
            objects.map((single: any, index: number) => {
              console.log('single', single);
              return (
                <Picker.Item
                  key={single.Id}
                  value={index}
                  label={single.Name}
                />
              );
            })}
        </Picker>
      )}
    </View>
  );
};

export default RelatedField;
