import React, { useState, useEffect } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import { Table, Row, Rows } from 'react-native-table-component';
// import { base } from '../airtable/index';

const ReportDay = ({ navigation, route }: any) => {
  const [table, setTable] = useState([]);
  const [loading, setLoading] = useState(true);
  const { title } = route.params;
  const tableHead = [
    'Name',
    'Address',
    'Time',
    'CHL',
    'CHL Add',
    'PH',
    'PH Add',
    'TA',
    'TA Add'
  ];
  const widthArr = [160, 140, 60, 70, 60, 50, 60, 50, 60];

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 16,
      paddingTop: 30,
      backgroundColor: '#fff'
    },
    header: { height: 50, backgroundColor: '#fff' },
    textHeader: { fontWeight: 'bold', textAlign: 'center', fontSize: 16 },
    text: { textAlign: 'center', fontSize: 16 },
    dataWrapper: { marginTop: -1 },
    row: { height: 60, backgroundColor: '#E7E6E1' }
  });

  const getRows = async () => {
    setLoading(true);
    let allRows = [];
    base('April 13-17')
      .select({
        view: 'Grid view'
      })
      .firstPage((err, records) => {
        if (err) {
          console.error(err);
          return;
        }

        records.forEach(record => {
          if (record.get('DAY') === title) {
            const singleRow = [
              record.get('NAME'),
              record.get('ADDRESS'),
              record.get('TIME'),
              record.get('CHLORINE'),
              record.get('CHL ADDED'),
              record.get('PH'),
              record.get('PH ADDED'),
              record.get('TA'),
              record.get('TA ADDED')
            ];
            allRows = [...allRows, singleRow];
          }
        });
        console.log('allRows', allRows);
        setTable(allRows);
      });
    setLoading(false);
  };
  useEffect(() => {
    getRows();
    console.log('tables', table);
  }, []);
  return (
    <View style={styles.container}>
      <ScrollView horizontal>
        <View>
          <Table borderStyle={{ borderWidth: 0, borderColor: '#C1C0B9' }}>
            <Row
              data={tableHead}
              style={styles.header}
              widthArr={widthArr}
              textStyle={styles.textHeader}
            />
          </Table>
          <ScrollView style={styles.dataWrapper}>
            <Table borderStyle={{ borderWidth: 0, borderColor: '#C1C0B9' }}>
              {table.map((rowData, index) => (
                <Row
                  key={index}
                  data={rowData}
                  widthArr={widthArr}
                  style={[styles.row, index % 2 && { backgroundColor: '#fff' }]}
                  textStyle={styles.text}
                />
              ))}
            </Table>
          </ScrollView>
        </View>
      </ScrollView>
    </View>
  );
};

export default ReportDay;
