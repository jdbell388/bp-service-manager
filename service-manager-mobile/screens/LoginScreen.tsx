import React from 'react';

import Login from '../components/auth/Login';

const LoginScreen = ({ navigation }: any) => {
  return <Login navigation={navigation} />;
};

export default LoginScreen;
