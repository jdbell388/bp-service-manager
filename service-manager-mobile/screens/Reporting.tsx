import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { ListItem } from 'react-native-elements';

const styles = StyleSheet.create({
  headerText: {
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingTop: 40,
    paddingBottom: 30
  },
  listItem: {
    padding: 20
  }
});

const Reporting = ({ navigation }: any) => {
  return (
    <View>
      <Text style={styles.headerText}>This Week</Text>
      <ListItem
        bottomDivider
        onPress={() =>
          navigation.navigate('ReportDay', {
            title: 'Monday'
          })
        }
        containerStyle={styles.listItem}
      >
        <ListItem.Content>
          <ListItem.Title>Monday</ListItem.Title>
        </ListItem.Content>
      </ListItem>
      <ListItem
        bottomDivider
        onPress={() =>
          navigation.navigate('ReportDay', {
            title: 'Tuesday'
          })
        }
        containerStyle={styles.listItem}
      >
        <ListItem.Content>
          <ListItem.Title>Tuesday</ListItem.Title>
        </ListItem.Content>
      </ListItem>
      <ListItem
        bottomDivider
        onPress={() =>
          navigation.navigate('ReportDay', {
            title: 'Wednesday'
          })
        }
        containerStyle={styles.listItem}
      >
        <ListItem.Content>
          <ListItem.Title>Wednesday</ListItem.Title>
        </ListItem.Content>
      </ListItem>
      <ListItem
        bottomDivider
        onPress={() =>
          navigation.navigate('ReportDay', {
            title: 'Thursday'
          })
        }
        containerStyle={styles.listItem}
      >
        <ListItem.Content>
          <ListItem.Title>Thursday</ListItem.Title>
        </ListItem.Content>
      </ListItem>
      <ListItem
        bottomDivider
        onPress={() =>
          navigation.navigate('ReportDay', {
            title: 'Friday'
          })
        }
        containerStyle={styles.listItem}
      >
        <ListItem.Content>
          <ListItem.Title>Friday</ListItem.Title>
        </ListItem.Content>
      </ListItem>
    </View>
  );
};

export default Reporting;
