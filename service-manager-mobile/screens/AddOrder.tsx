import React from 'react';
import { View, Text } from 'react-native';

import OrderForm from '../components/workorders/OrderForm';
import { addOrder } from '../firebase/workorders';

const AddOrder = (props: any) => {
  const { navigation } = props;

  const submitForm = async (
    Title: any,
    Description: any,
    Employee: any,
    Account: any
  ) => {
    const add = await addOrder({ Title, Description, Employee, Account });
    alert(`Order Added, ${Title}, ${Description}, ${Employee}, ${Account}`);
    navigation.navigate('WorkOrders');
    return add;
  };

  return (
    <View>
      <Text>Add Work Order</Text>
      <OrderForm submit={submitForm} />
    </View>
  );
};

export default AddOrder;
