import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import LoginView from '../views/LoginView.vue';
import RegisterView from '../views/RegisterView.vue';
import DashboardView from '../views/DashboardView.vue';
import ShellView from '../views/ShellView.vue';
import NewCustomer from '../views/NewCustomer.vue';
import EditCustomer from '../views/EditCustomer.vue';
import CustomerTable from '@/components/CustomerTable.vue';
import EmployeesView from '../views/EmployeesView.vue';
import NewEmployee from '../views/NewEmployee.vue';
import EditEmployee from '../views/EditEmployee.vue';
import EmployeeTable from '@/components/EmployeeTable.vue';
import RoutesView from '../views/RoutesView.vue';
import RoutesTable from '@/components/RoutesTable.vue';
import EditRoute from '../views/EditRoute.vue';
import ReportsView from '../views/ReportsView.vue';
import ReportsTable from '@/components/ReportsTable.vue';
import NewReport from '../views/NewReport.vue';
import WorkordersView from '../views/WorkordersView.vue';
import NewWorkorder from '../views/NewWorkorder.vue';
import EditWorkorder from '../views/EditWorkorder.vue';
import WorkorderTable from '@/components/WorkorderTable.vue';
import ViewSingleReport from '@/views/ViewSingleReport.vue';
import { supabase } from '@/services/databaseService';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/login',
    name: 'login',
    component: LoginView,
    meta: {
      requiresNoAuth: true,
    },
  },
  {
    path: '/register',
    name: 'Register',
    component: RegisterView,
    meta: {
      requiresNoAuth: true,
    },
  },
  {
    path: '/',
    name: 'Dashboard',
    component: DashboardView,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/customers',
    component: ShellView,
    children: [
      {
        path: '',
        component: CustomerTable,
      },
      {
        path: 'new',
        component: NewCustomer,
      },
      {
        path: 'edit/:id',
        component: EditCustomer,
      },
    ],
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/employees',
    component: EmployeesView,
    children: [
      {
        path: '',
        component: EmployeeTable,
      },
      {
        path: 'new',
        component: NewEmployee,
      },
      {
        path: 'edit/:id',
        component: EditEmployee,
      },
    ],
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/routes',
    component: RoutesView,
    children: [
      {
        path: '',
        component: RoutesTable,
      },
      {
        path: 'edit',
        component: EditRoute,
      },
      {
        path: 'edit/:id',
        component: EditRoute,
      },
      {
        path: 'employee/:id',
        component: RoutesTable,
      },
    ],
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/reports',
    component: ReportsView,
    children: [
      {
        path: '',
        component: ReportsTable,
      },
      {
        path: 'new',
        component: NewReport,
      },
      {
        path: 'employee/:id',
        component: ReportsTable,
      },
      {
        path: 'view/:id',
        component: ViewSingleReport,
      },
    ],
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/workorders',
    component: WorkordersView,
    children: [
      {
        path: '',
        component: WorkorderTable,
      },
      {
        path: 'new',
        component: NewWorkorder,
      },
      {
        path: 'edit/:id',
        component: EditWorkorder,
      },
    ],
    meta: {
      requiresAuth: true,
    },
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

const checkForSession = async () => {
  try {
    const { data, error } = await supabase.auth.getSession();
    if (error) {
      throw new Error(error.message);
    }
    if (data.session) return true;
  } catch (e) {
    console.error('Error with Session: ', e);
  }

  return false;
};

router.beforeEach(async (to, from, next) => {
  try {
    const session = await checkForSession();
    if (to.meta.requiresAuth && !session) {
      next('/login');
    } else if (to.meta.requiresNoAuth && session) {
      next('/dashboard');
    } else {
      next();
    }
  } catch (e) {
    console.error('Error with Session: ', e);
  }
});

export default router;
