import { supabase } from './databaseService';

export interface EmployeeInt {
  id?: any;
  firstName?: string;
  lastName?: string;
  address?: string;
  email?: string;
  phone?: string;
  authId?: string;
  role?: string;
}

export const addEmployee = async (employee: EmployeeInt) => {
  const { data, error } = await supabase.from('employees').insert(employee).select().single();

  if (error) throw error;
  return data;
};

export const getAllEmployees = async () => {
  const { data, error } = await supabase.from('employees').select('*');

  if (error) throw error;
  return data;
};

export const getEmployeeById = async (id: string) => {
  const { data, error } = await supabase.from('employees').select('*').eq('id', id).single();

  if (error) throw error;
  return data;
};

export const getEmployeeByEmail = async (email: string) => {
  const { data, error } = await supabase.from('employees').select('*').eq('email', email).single();

  if (error) throw error;
  return data;
};

export const updateEmployee = async (id: string, updates: Partial<EmployeeInt>) => {
  const { data, error } = await supabase.from('employees').update(updates).eq('id', id).select().single();

  if (error) throw error;
  return data;
};

export const deleteEmployee = async (id: string) => {
  const { error } = await supabase.from('employees').delete().eq('id', id);

  if (error) throw error;
  return true;
};

export const employeeColumns = [
  {
    label: 'First Name',
    field: 'firstName',
  },
  {
    label: 'Last Name',
    field: 'lastName',
  },
  {
    label: 'Address',
    field: 'address',
  },
  {
    label: 'Email',
    field: 'email',
  },
  {
    label: 'Phone',
    field: 'phone',
  },
  {
    label: 'Actions',
    field: 'Actions',
  },
];
