import { supabase } from './databaseService';

export interface AccountInt {
  address?: string;
  email?: string;
  employeeId?: string;
  firstName?: string;
  lastName?: string;
  phone?: string;
  id?: string;
  filterSizes?: string;
  service?: string;
  neighborhood?: string;
  sideOfHouse?: string;
  gateCodesKeys?: string;
  salt?: string;
}

export const addAccount = async (account: AccountInt) => {
  const { data, error } = await supabase.from('accounts').insert(account).select().single();

  if (error) throw error;
  return data;
};

export const getAllAccounts = async () => {
  const { data, error } = await supabase.from('accounts').select('*');

  if (error) throw error;
  return data;
};

export const getAccountById = async (id: string) => {
  const { data, error } = await supabase.from('accounts').select('*').eq('id', id).single();

  if (error) throw error;
  return data;
};

export const updateAccount = async (id: string, updates: Partial<AccountInt>) => {
  const { data, error } = await supabase.from('accounts').update(updates).eq('id', id).select().single();

  if (error) throw error;
  return data;
};

export const deleteAccount = async (id: string) => {
  const { error } = await supabase.from('accounts').delete().eq('id', id);

  if (error) throw error;
  return true;
};

export const accountColumns = [
  {
    label: 'First Name',
    field: 'firstName',
  },
  {
    label: 'Last Name',
    field: 'lastName',
  },
  {
    label: 'Address',
    field: 'address',
  },
  {
    label: 'Email',
    field: 'email',
  },
  {
    label: 'Employee',
    field: 'employeeName',
  },
  {
    label: 'Phone',
    field: 'phone',
  },
  {
    label: 'Filter Size',
    field: 'filterSizes',
  },
  {
    label: 'Service',
    field: 'service',
  },
  {
    label: 'Neighborhood',
    field: 'neighborhood',
  },
  {
    label: 'Side of House',
    field: 'sideOfHouse',
  },
  {
    label: 'Gate Codes/Keys',
    field: 'gateCodesKeys',
  },
  {
    label: 'Salt',
    field: 'salt',
  },
  {
    label: 'Actions',
    field: 'actions',
  },
];
