import { supabase } from './databaseService';

export const session = async () => await supabase.auth.getSession();

export const getUser = async () => (await supabase.auth.getUser()).data.user;

export const signOut = async () => await supabase.auth.signOut();

export const signIn = async (email: string, password: string) =>
  await supabase.auth.signInWithPassword({ email, password });

export const signUp = async (email: string, password: string) => await supabase.auth.signUp({ email, password });

export const getEmployeeByAuthId = async (authId: string) => {
  const { data, error } = await supabase.from('employees').select('*').eq('authId', authId).single();

  if (error) throw error;
  return data;
};

export const getCurrentEmployee = async () => {
  const user = await getUser();
  if (!user) throw new Error('No user logged in');

  const { data, error } = await supabase.from('employees').select('*').eq('authId', user.id).single();

  if (error) throw error;
  return data;
};
