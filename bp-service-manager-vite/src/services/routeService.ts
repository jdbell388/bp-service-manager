import { supabase } from './databaseService';

export interface RouteSingleInt {
  customerAddress: string;
  customerName: string;
}

export interface CustInt {
  id: string;
  name: string;
  address: string;
}

export interface RouteInt {
  id?: string;
  day: string;
  route?: CustInt[];
  employeeId?: string;
}

export const addRoute = async (route: RouteInt) => {
  const { data, error } = await supabase.from('routes').insert(route).select().single();

  if (error) throw error;
  return data;
};

export const getAllRoutes = async () => {
  const { data, error } = await supabase.from('routes').select('*');

  if (error) throw error;
  return data;
};

export const getRouteById = async (id: string) => {
  const { data, error } = await supabase.from('routes').select('*').eq('id', id).single();

  if (error) throw error;
  return data;
};

export const getRoutesByEmployee = async (id: string) => {
  const { data, error } = await supabase.from('routes').select('*').eq('employeeId', id);

  if (error) throw error;
  return data;
};

export const getRoutesByDayAndEmployee = async (id: string, day: string) => {
  const { data, error } = await supabase.from('routes').select('*').eq('employeeId', id).eq('day', day);

  if (error) throw error;
  return data;
};

export const updateRoute = async (id: string, updates: Partial<RouteInt>) => {
  const { data, error } = await supabase.from('routes').update(updates).eq('id', id).select().single();

  if (error) throw error;
  return data;
};

export const deleteRoute = async (id: string) => {
  const { error } = await supabase.from('routes').delete().eq('id', id);

  if (error) throw error;
  return true;
};

export const routeColumns = [
  {
    label: 'Day',
    field: 'day',
  },
  {
    label: 'Employee Id',
    field: 'employeeId',
  },
  {
    label: 'Employee Name',
    field: 'employeeName',
  },
  {
    label: 'Route',
    field: 'route',
  },
];
