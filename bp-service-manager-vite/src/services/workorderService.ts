import { supabase } from './databaseService';

export interface WorkorderInt {
  id?: string;
  name?: string;
  description?: string;
  employeeId?: string;
  accountId?: string;
  status?: string;
  tags?: string[];
  created_at?: string;
}

export const addWorkorder = async (workorder: WorkorderInt) => {
  const { data, error } = await supabase.from('workorders').insert(workorder).select().single();

  if (error) throw error;
  return data;
};

export const getAllWorkorders = async () => {
  const { data, error } = await supabase.from('workorders').select('*');

  if (error) throw error;
  return data;
};

export const getAllOpenWorkorders = async () => {
  const { data, error } = await supabase.from('workorders').select('*').neq('status', 'completed');

  if (error) throw error;
  return data;
};

export const getWorkorderById = async (id: string) => {
  const { data, error } = await supabase.from('workorders').select('*').eq('id', id).single();

  if (error) throw error;
  return data;
};

export const updateWorkorder = async (id: string, updates: Partial<WorkorderInt>) => {
  const { data, error } = await supabase.from('workorders').update(updates).eq('id', id).select().single();

  if (error) throw error;
  return data;
};

export const deleteWorkorder = async (id: string) => {
  const { error } = await supabase.from('workorders').delete().eq('id', id);

  if (error) throw error;
  return true;
};

export const markWorkorderComplete = async (id: string) => {
  const { data, error } = await supabase
    .from('workorders')
    .update({ status: 'completed' })
    .eq('id', id)
    .select()
    .single();

  if (error) throw error;
  return data;
};

export const workorderColumns = [
  {
    label: 'Title',
    field: 'title',
  },
  {
    label: 'Description',
    field: 'description',
  },
  {
    label: 'Employee',
    field: 'employeeName',
    filterOptions: {
      enabled: true,
      placeholder: 'Filter By Employee',
    },
  },
  {
    label: 'Customer',
    field: 'customerName',
  },
  {
    label: 'Status',
    field: 'status',
  },
  {
    label: 'Actions',
    field: 'actions',
  },
];
