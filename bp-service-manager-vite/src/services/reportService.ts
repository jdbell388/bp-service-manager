import { supabase } from './databaseService';

export interface ReportInt {
  id?: any;
  address?: string;
  chlAdded?: string;
  chlorine?: string;
  customerId?: string;
  employeeId?: string;
  ph?: string;
  phAdded?: string;
  ta?: string;
  taAdded?: string;
  calcium?: string;
  calciumAdded?: string;
  cya?: string;
  cyaAdded?: string;
  miscAdded?: string;
  filterCleaned?: boolean;
  time?: string;
  notes?: string;
  image?: File;
  imageUrl?: string;
}

export const addReport = async (report: ReportInt) => {
  const { data, error } = await supabase.from('reports').insert(report).select().single();

  if (error) throw error;
  return data;
};

export const getAllReports = async () => {
  const { data, error } = await supabase.from('reports').select('*');

  if (error) throw error;
  return data;
};

export const getReportById = async (id: string) => {
  const { data, error } = await supabase.from('reports').select('*').eq('id', id).single();

  if (error) throw error;
  return data;
};

export const getReportsByEmployee = async (employeeId: string) => {
  const { data, error } = await supabase.from('reports').select('*').eq('employeeId', employeeId);

  if (error) throw error;
  return data;
};

export const updateReport = async (id: string, updates: Partial<ReportInt>) => {
  const { data, error } = await supabase.from('reports').update(updates).eq('id', id).select().single();

  if (error) throw error;
  return data;
};

export const deleteReport = async (id: string) => {
  const { error } = await supabase.from('reports').delete().eq('id', id);

  if (error) throw error;
  return true;
};

export const reportColumns = [
  {
    label: 'Cust',
    field: 'customerName',
    filterOptions: {
      enabled: true,
      placeholder: 'Filter By Customer',
    },
  },
  {
    label: 'Emp',
    field: 'employeeName',
    filterOptions: {
      enabled: true,
      placeholder: 'Filter By Employee',
    },
  },
  {
    label: 'Address',
    field: 'address',
  },
  {
    label: 'CHL',
    field: 'chlorine',
  },
  {
    label: 'CHL ADD',
    field: 'chlAdded',
  },
  {
    label: 'PH',
    field: 'ph',
  },
  {
    label: 'PH ADD',
    field: 'phAdded',
  },
  {
    label: 'TA',
    field: 'ta',
  },
  {
    label: 'TA ADD',
    field: 'taAdded',
  },
  {
    label: 'Calcium',
    field: 'calcium',
  },
  {
    label: 'Cal ADD',
    field: 'calciumAdded',
  },
  {
    label: 'CYA',
    field: 'cya',
  },
  {
    label: 'CYA ADD',
    field: 'cyaAdded',
  },
  {
    label: 'Misc Additions',
    field: 'miscAdded',
  },
  {
    label: 'Filter Cleaned',
    field: 'filterCleaned',
  },
  {
    label: 'Date',
    field: 'time',
  },
  {
    label: 'Notes',
    field: 'notes',
  },
  {
    label: 'Actions',
    field: 'actions',
  },
];
