module.exports = {
  printWidth: 120,
  semi: true,
  arrowParens: 'avoid',
  singleQuote: true,
  trailingComma: 'all',
  // Prettier Import Configs
  // prettier-ignore
  importOrder: [
    // Service files
    '^.*service.*$',
    // All other files starting with ./ or @
    '^([./]|@/(.*)$)',
  ],
  importOrderSeparation: true,
  formatOnSave: true,
  importOrder: ["^.*service.*$", "^([./]|@/(.*)$)"],
};